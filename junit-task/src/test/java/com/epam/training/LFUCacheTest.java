package com.epam.training;


import com.epam.training.impl.LFUCache;

import org.junit.Test;


import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;


import static org.junit.Assert.*;


public class LFUCacheTest {
    private LFUCache<Integer, String> cache;

    @Test
    public void get() {
        cache = new LFUCache<>(2);
        cache.put(1, "b");
        cache.put(2, "a");
        assertEquals("a", cache.get(2));
        cache.put(3, "d");
        assertNull(cache.get(1));

    }


    @Test
    public void getAll() {
        cache = new LFUCache<>(2);
        List<String> list = new ArrayList<>(Arrays.asList("b", "c"));
        cache.put(1, "a");
        cache.put(2, "b");
        cache.get(2);
        cache.put(3, "c");
        assertEquals(list, cache.getAll());
    }

    @Test
    public void put() {
        cache = new LFUCache<>(2);
        assertNull(cache.put(1, "a"));
        assertNull(cache.put(2, "b"));
        cache.get(2);
        assertNull(cache.put(3, "putTest"));
        assertEquals("putTest", cache.put(3, "c"));
    }

    @Test
    public void remove() {
        cache = new LFUCache<>(4);
        List<String> list = new ArrayList<>(Arrays.asList("b", "c", "d"));

        cache.put(1, "a");
        cache.put(2, "b");
        cache.put(3, "c");
        cache.put(4, "d");
        cache.remove(1);
        assertEquals(list, cache.getAll());

    }
}
