package com.epam.training;


import com.epam.training.impl.LRUCache;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class LRUCacheTest {

    private LRUCache<Integer, String> cache;

    @Test
    public void get() {
        cache = new LRUCache<>(2);
        cache.put(1, "a");
        cache.put(2, "b");
        assertEquals("b", cache.get(2));
        cache.put(3, "c");
        assertNull(cache.get(1));
        assertEquals("c", cache.get(3));
    }

    @Test
    public void getAll() {
        cache = new LRUCache<>(2);
        List<String> list = new ArrayList<>(Arrays.asList("b", "c"));
        cache.put(1, "a");
        cache.put(2, "b");
        cache.put(3, "c");
        assertEquals(list, cache.getAll());
    }

    @Test
    public void put() {
        cache = new LRUCache<>(2);
        List<String> list = new ArrayList<>(Arrays.asList("a", "b"));
        assertNull(cache.put(1, "a"));
        assertNull(cache.put(2, "b"));
        assertNull(cache.put(3, "putTest"));
        assertNotEquals(list, cache.getAll());
        assertNotNull(cache.get(2));
        assertEquals("putTest", cache.put(3, "c"));

    }

    @Test
    public void remove() {
        cache = new LRUCache<>(4);
        List<String> list = new ArrayList<>(Arrays.asList("b", "c", "d"));
        List<String> stringList = new ArrayList<>(Arrays.asList("b", "c", "d", "a"));
        List<String> arrayList = new ArrayList<>(Arrays.asList("c", "d", "a", "e"));
        cache.put(1, "a");
        cache.put(2, "b");
        cache.put(3, "c");
        cache.put(4, "d");
        cache.remove(1);
        assertEquals(list, cache.getAll());
        cache.put(1, "a");
        assertEquals(stringList, cache.getAll());
        cache.put(5, "e");
        assertEquals(arrayList, cache.getAll());


    }
}
