package com.epam.training.beans;


public class CacheEntry<Value> {
    private final Value value;
    private int frequency;

    public CacheEntry(Value value) {
        this.value = value;
        this.frequency = 1;
    }

    public Value getValue() {
        return value;
    }

    public int getFrequency() {
        return frequency;
    }

    public CacheEntry incrementFrequency() {
        ++frequency;
        return this;
    }

    @Override
    public String toString() {
        return value.toString();

    }
}
