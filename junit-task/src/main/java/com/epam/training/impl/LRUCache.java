package com.epam.training.impl;

import com.epam.training.interfaces.Cache;

import java.util.*;


public class LRUCache<Key, Value> implements Cache<Key, Value> {
    private final LinkedHashMap<Key, Value> map;


    public LRUCache(int limit) {
        if (limit <= 0) {
            throw new IllegalArgumentException("Capacity should be more than 0");
        }
        this.map = new LinkedHashMap<Key, Value>(limit, 1, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Key, Value> eldest) {
                return this.size() > limit;
            }
        };
    }

    @Override
    public Value get(Key key) {
        return map.get(key);
    }

    @Override
    public List<Value> getAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public Value put(Key key, Value value) {
        return map.put(key, value);
    }

    @Override
    public void remove(Key key) {
        map.remove(key);
    }

    @Override
    public String toString() {
        return map.toString();
    }
}