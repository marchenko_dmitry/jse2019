package com.epam.training.impl;


import com.epam.training.beans.CacheEntry;
import com.epam.training.interfaces.Cache;

import java.util.*;

public class LFUCache<Key, Value> implements Cache<Key, Value> {

    private final LinkedHashMap<Key, CacheEntry> storage;
    private final int capacity;

    public LFUCache(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Capacity should be more than 0");
        }
        this.capacity = capacity;
        this.storage = new LinkedHashMap<>(capacity, 1);
    }

    @Override
    public Value get(Key key) {
        CacheEntry<Value> cacheEntry = storage.get(key);
        if (cacheEntry == null) {
            return null;
        }
        return (Value) cacheEntry.incrementFrequency().getValue();
    }

    @Override
    public List<Value> getAll() {
        List<Value> list = new ArrayList<>();
        storage.forEach((key, value) -> list.add((Value) value.incrementFrequency().getValue()));
        return list;
    }

    @Override
    public Value put(Key key, Value value) {
        doEvictionIfNeeded(key);
        CacheEntry<Value> oldCacheEntry = storage.put(key, new CacheEntry(value));
        if (oldCacheEntry == null) {
            return null;
        }
        return oldCacheEntry.getValue();
    }

    @Override
    public void remove(Key key) {
        storage.remove(key);
    }


    private void doEvictionIfNeeded(Key putKey) {
        if (storage.size() < capacity) {
            return;
        }
        long minFrequency = Long.MAX_VALUE;
        Key keyToRemove = null;
        if (storage.containsKey(putKey)) return;
        for (Map.Entry<Key, CacheEntry> entry : storage.entrySet()) {
            //no eviction required cause element already exists, we just need to replace it
            if (minFrequency >= entry.getValue().getFrequency()) {
                minFrequency = entry.getValue().getFrequency();
                keyToRemove = entry.getKey();
            }
        }
        storage.remove(keyToRemove);
    }


}

