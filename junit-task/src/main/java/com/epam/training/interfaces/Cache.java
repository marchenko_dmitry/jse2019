package com.epam.training.interfaces;


import java.util.List;

public interface Cache<Key, Value> {

    Value get(Key key);

    List<Value> getAll();

    Value put(Key key, Value value);

    void remove(Key key);
}
