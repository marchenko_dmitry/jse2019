package com.epam.training;

/**
 * class which print sentence.
 */
public final class Main {
    /**
     * no args constructor.
     */
    private Main() {
        //not called
    }

    /**
     * simple method which print sentence.
     * @param args - input value
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!");
    }
}
