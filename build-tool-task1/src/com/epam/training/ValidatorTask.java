package com.epam.training;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Logger;

/**
 * class which have checkers.
 */
public class ValidatorTask extends Task {
    private static final Logger logger = Logger.getLogger(ValidatorTask.class.getName());
    /**
     * elements for checked dependencies.
     */
    private boolean checkdepends;
    /**
     * elements for checked defaults.
     */
    private boolean checkdefault;
    /**
     * elements for checked names.
     */
    private boolean checknames;

    /**
     * setter.
     *
     * @param checkdepend - check dependencies
     */

    public void setCheckdepends(final boolean checkdepend) {
        this.checkdepends = checkdepend;
    }

    /**
     * setter.
     *
     * @param checkdefaults - check defaults
     */
    public void setCheckdefault(final boolean checkdefaults) {
        this.checkdefault = checkdefaults;
    }

    /**
     * setter.
     *
     * @param checkname - check name
     */
    public void setChecknames(final boolean checkname) {
        this.checknames = checkname;
    }

    /**
     * elements for checked dependencies.
     */
    private boolean isCheckdepends() {
        return checkdepends;
    }

    /**
     * elements for checked defaults.
     */
    private boolean isCheckdefault() {
        return checkdefault;
    }

    /**
     * elements for checked names.
     */
    private boolean isChecknames() {
        return checknames;
    }

    ;

    /**
     * method which check names.
     *
     * @param targetHashtable - hashtable of targets
     */
    private void checkNameMethod(
            final Hashtable<String, Target> targetHashtable) {
        if (isChecknames()) {
            if (!targetHashtable.values().stream()
                    .allMatch(x -> x.toString().matches("^[a-zA-Z-]*$")))
                logger.info("don't contains only letters");
        }
    }

    /**
     * method which check defaults.
     *
     * @param project - project which used
     */
    private void checkDefaultMethod(final Project project) {
        if (isCheckdefault()) {
            if (project.getDefaultTarget() == null )
                logger.info("don't contains default methods");
        }
    }

    /**
     * method which check dependencies.
     *
     * @param targetHashtable - hashtable of targets
     */
    private void checkDependsMethod(
            final Hashtable<String, Target> targetHashtable, Project project) {
        String defaultTarget = project.getDefaultTarget();
        if (isCheckdepends())
            if (!targetHashtable.containsKey("main"))
                logger.info("don't contain main target");
        for (Target target : targetHashtable.values()) {
            Enumeration dependencies = target.getDependencies();
            if (dependencies.hasMoreElements() && !target.getName().equals(defaultTarget) )
                logger.info( "don't contains depencensies");

        }
    }

    /**
     * method which contain three checker methods.
     *
     * @param targetHashtable - hashtable of targets
     * @param project         - project which used
     * @param filePath        - path to file
     */
    private void checkAll(final Hashtable<String, Target> targetHashtable,
                          final Project project, final String filePath) {
        checkNameMethod(targetHashtable);
        checkDefaultMethod(project);
        checkDependsMethod(targetHashtable, project);
        if (isChecknames() && isCheckdefault() && isCheckdepends()) {
            logger.info("file: " + filePath + " is correct");
        } else {
            logger.info("file: " + filePath + " - isn't correct");
        }
    }


    /**
     * method which build project.
     */
    public void execute() {
        for (Buildfile buildfile : buildfiles) {
            String filePath = buildfile.getTest();

            log("File " + filePath);
            Project project = new Project();

            ProjectHelper.getProjectHelper().parse(project, new File(filePath));
            Hashtable<String, Target> targetHashtable = project.getTargets();
            checkAll(targetHashtable, project, filePath);
        }
    }


    /**
     * collection for build files.
     */
    private List<Buildfile> buildfiles = new ArrayList<>();

    /**
     * method which create Build file.
     *
     * @return buildfile
     */
    public Buildfile createBuildfile() {
        Buildfile buildfile = new Buildfile();
        buildfiles.add(buildfile);
        return buildfile;
    }

    public class Buildfile {
        Buildfile() {
        }

        /**
         * elements for test.
         */
        private String test;

        /**
         * constructor.
         *
         * @param temp - some element for test
         */
        public Buildfile(final String temp) {
            this.test = temp;
        }

        /**
         * setter.
         *
         * @param temp - any string
         */
        public void setTest(final String temp) {
            this.test = temp;
        }

        /**
         * getter.
         *
         * @return test
         */
        String getTest() {
            return test;
        }
    }
}
