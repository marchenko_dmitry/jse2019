We have building with floors and elevator with certain capacity.
Initial data (number of floors, number of passengers, elevator capacity) placed in config.property file.
Start tasks.
ElevatorTask flow:
Elevator is moving among floors in cycle.
Elevator stops at every floor.
Elevator cabin has limit capacity.
Elevator asks passengers to exit from elevator cabin when reach floor.
Elevator asks passengers to enter from elevator cabin.
Actions performed in cycle till the moment when will be true all following conditions:
All dispatcher containers should be empty,
Elevator cabin should be empty,
All passengers should be in Complete state,
Each passenger will be in proper arrival floor (in each arrival floor passengers destination property should be equal floor number),
Total number of passengers should be equal sum of passengers in all arrival containers
Actions for starting, moving elevator, boarding and deboarding passengers are logged with log4j.
Main class starts application.
