package by.epam.training;

import by.epam.training.concurrency.Runner;
import by.epam.training.concurrency.model.Building;
import by.epam.training.concurrency.model.Floor;
import by.epam.training.concurrency.model.Passenger;
import by.epam.training.concurrency.validators.Validator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * unit test for init passengers
 */
public class InitPassTest {
    @Test
    public void initTest() {
        Building building = new Building(2, 1);
        Assert.assertEquals(2, building.getFloors().size());
        Assert.assertEquals(1, building.getElevator().getCapacity());
    }

    @Test
    public void isElevatorEmptyTest() {
        Validator mockValidator = mock(Validator.class);
        when(mockValidator.isElevatorEmpty()).thenReturn(false);
        Assert.assertFalse(mockValidator.isElevatorEmpty());
    }


    @Test
    public void checkConditionsTest() {
        Validator mockValidator = mock(Validator.class);
        when(mockValidator.checkConditions()).thenReturn(false);
        Assert.assertFalse(mockValidator.checkConditions());

        when(mockValidator.checkConditions()).thenReturn(true);
        Assert.assertTrue(mockValidator.checkConditions());

    }

}
