package by.epam.training;

import by.epam.training.concurrency.model.Building;
import by.epam.training.concurrency.model.Elevator;
import by.epam.training.concurrency.tasks.ElevatorMovementTask;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for moving elevator
 */
public class MovingElevatorTest {
    @Test
    public final void moveElevatorTest() {
        final Building building = new Building(5, 1);
        ElevatorMovementTask elevatorMovementTask = new ElevatorMovementTask(building, 5);

        final Elevator elevator = building.getElevator();
        final int curFloorIndex = elevator.getCurFloorIndex();
        assertThat(curFloorIndex, is(0));

        elevatorMovementTask.nextFloor();
        Assert.assertEquals(1, curFloorIndex);
        elevatorMovementTask.nextFloor();
        Assert.assertEquals(2, curFloorIndex);
        elevatorMovementTask.nextFloor();
        Assert.assertEquals(3, curFloorIndex);
        elevatorMovementTask.nextFloor();
        Assert.assertEquals(4, curFloorIndex);
        elevatorMovementTask.nextFloor();
        Assert.assertEquals(3, curFloorIndex);
        elevatorMovementTask.nextFloor();
        Assert.assertEquals(2, curFloorIndex);


    }
}
