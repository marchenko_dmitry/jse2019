package by.epam.training.concurrency.controllers;

import by.epam.training.concurrency.constants.StrConstants;
import by.epam.training.concurrency.model.Elevator;
import by.epam.training.concurrency.model.Floor;
import by.epam.training.concurrency.model.Passenger;
import org.apache.log4j.Logger;

/**
 * Class controller which check who can enter to elevator
 */
public class Controller {
    /**
     * logger
     */
    private static final Logger logger = Logger.getLogger(Controller.class);
    private static final String PASSENGER = "passenger";

    /**
     * method which allow passenger to elevator from source floor
     *
     * @param passenger - passenger
     * @param elevator  - elevator
     * @return result
     */
    public boolean enter(Passenger passenger, final Elevator elevator) {
        final boolean result = elevator.addPassenger(passenger);
        if (result) {
            Floor curFloor = elevator.getCurFloor();
            curFloor.removeDispatchPassenger(passenger);
            int sourceFloor = passenger.getSourceFloor();
            final int id = passenger.getId();
            logger.info(Controller.PASSENGER + StrConstants.WHITESPACE + id
                    + StrConstants.ON_STORY + sourceFloor);
        }
        return result;
    }

    /**
     * method which allow passenger from elevator to destination floor
     *
     * @param passenger - passenger
     * @param elevator  - elevator
     * @return result
     */
    public void exit(final Passenger passenger, final Elevator elevator) {
        elevator.removePassenger(passenger);
        final Floor curFloor = elevator.getCurFloor();
        curFloor.addArrivalPassenger(passenger);
        final int destinationFloor = passenger.getDestinationFloor();
        logger.info(Controller.PASSENGER + StrConstants.WHITESPACE + passenger.getId()
                + StrConstants.ON_STORY + destinationFloor);
    }


}
