package by.epam.training.concurrency.enums;

/**
 * enum ElevatorAction which describe elevator action
 */
public enum ElevatorAction {
    TRANSPORTATION_IS_START,
    TRANSPORTATION_IS_SUCCESS,
    ELEVATOR_IS_MOVE,
    PASSENGER_IS_BOARDING,
    PASSENGER_IS_DEBOARDING,
}
