package by.epam.training.concurrency.readers;

import by.epam.training.concurrency.constants.StrConstants;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class ReaderParameters which read parameters from file
 */
public class ReaderParameters {
    /**
     * items declaration
     */
    private int floorNumber;
    private int elevatorCapacity;
    private int numberOfPassengers;
    private String fileName;
    /**
     * logger
     */
    private static final Logger logger = Logger.getLogger(ReaderParameters.class);

    /**
     * costructor with args
     *
     * @param fileName - name of file
     */
    public ReaderParameters(final String fileName) {
        this.fileName = fileName;
        readConfigFile();
    }

    /**
     * getter
     *
     * @return number of floor
     */
    public int getFloorNumber() {
        return floorNumber;
    }

    /**
     * getter
     *
     * @return capacity of elevator
     */
    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    /**
     * getter
     *
     * @return number of passengers
     */
    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    /**
     * method which read config file
     */
    private void readConfigFile() {
        Properties properties = new Properties();

        try (InputStream inputStream = new FileInputStream(fileName)) {
            properties.load(inputStream);
            floorNumber = Integer.parseInt(properties.getProperty(StrConstants.FLOORS_NUMBER));
            elevatorCapacity = Integer.parseInt(properties.getProperty(StrConstants.ELEVATOR_CAPACITY));
            numberOfPassengers = Integer.parseInt(properties.getProperty(StrConstants.PASSENGERS_NUMBER));
        } catch (NumberFormatException e) {
            logger.error(e.getMessage() + " parameter value must be a number");
        } catch (IOException e) {
            logger.error("File " + fileName + " not found");
        }
    }
}

