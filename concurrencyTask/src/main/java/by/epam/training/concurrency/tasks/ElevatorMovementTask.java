package by.epam.training.concurrency.tasks;

import by.epam.training.concurrency.constants.StrConstants;
import by.epam.training.concurrency.controllers.Controller;
import by.epam.training.concurrency.enums.ElevatorAction;
import by.epam.training.concurrency.model.Building;
import by.epam.training.concurrency.model.Elevator;
import by.epam.training.concurrency.model.Floor;
import by.epam.training.concurrency.model.Passenger;
import by.epam.training.concurrency.validators.Validator;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Class ElevatorMovementTask used elevator like a thread
 */
public class ElevatorMovementTask implements Runnable {
    /**
     * item declaration
     */
    private final Elevator elevator;
    private final List<Floor> floors;
    private ElevatorAction action;
    private Floor currentFloor;
    private Validator validator;


    /**
     * logger
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * constructor with args
     *
     * @param building - building
     */
    public ElevatorMovementTask(final Building building, final int numbOfPass) {
        elevator = building.getElevator();
        floors = building.getFloors();
        currentFloor = elevator.getCurFloor();
        validator = new Validator(building, numbOfPass);
    }

    /**
     * method which move elevator to next floor
     */
    public void nextFloor() {
        action = ElevatorAction.ELEVATOR_IS_MOVE;
        int from = elevator.getCurFloor().getNumber();
        elevator.moveElevator();
        int to = elevator.getCurFloor().getNumber();
        currentFloor = elevator.getCurFloor();
        logger.info(action + StrConstants.FROM + from + StrConstants.TO + to);
    }

    /**
     * method which deboard passegers from elevator
     */
    private void deboardFromElevator() {
        action = ElevatorAction.PASSENGER_IS_DEBOARDING;
        int count = 0;
        for (Passenger passenger : elevator.getPassengersContainer()) {
            if (currentFloor.getNumber() == passenger.getDestinationFloor()) {
                count++;
            }
        }
        if (count > 0) {
            currentFloor.getLock().lock();
            try {
                currentFloor.getExitCondition().signalAll();
                try {
                    currentFloor.getCompleted().await();
                    logger.info(action + StrConstants.FROM + currentFloor.getNumber());
                } catch (InterruptedException e) {
                    logger.info("Thread " + currentFloor.getNumber() + " freed");
                }
            } finally {
                currentFloor.getLock().unlock();
            }
        }
    }

    /**
     * method which boarding passengers to elevator
     */
    private void boardToElevator() {
        action = ElevatorAction.PASSENGER_IS_BOARDING;
        int count = 0;
        for (Passenger passenger : currentFloor.getDispatchContainer()) {
            if (currentFloor.getNumber() == passenger.getSourceFloor()) {
                count++;
            }
        }
        if (count > 0) {
            currentFloor.getLock().lock();
            try {
                currentFloor.getEnterCondition().signalAll();
                try {
                    currentFloor.getCompleted().await();
                    logger.info(action + StrConstants.TO + currentFloor.getNumber());
                } catch (InterruptedException e) {
                    logger.info(e.getMessage());
                }
            } finally {
                currentFloor.getLock().unlock();
            }
        }
    }

    /**
     * method which manage elevator
     */
    public void run() {
        action = ElevatorAction.TRANSPORTATION_IS_START;
        logger.info(action);
        while (!transportationIsFinished()) {
            deboardFromElevator();
            if (transportationIsFinished()) {
                action = ElevatorAction.TRANSPORTATION_IS_SUCCESS;
                logger.info(action);
                validator.validateTransportation();
                return;
            }
            boardToElevator();
            nextFloor();
        }


    }

    /**
     * method which check that transportation is finished
     *
     * @return true if is finished, false if not
     */
    private boolean transportationIsFinished() {
        if (!elevator.getPassengersContainer().isEmpty()) {
            return false;
        }
        for (Floor floor : floors) {
            if (!floor.getDispatchContainer().isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
