package by.epam.training.concurrency.generators;

import java.util.Random;

/**
 * Class FloorGenerator which generate random number
 * of source and destination floors for passenger
 */
public class FloorsGenerator {
    /**
     * item declaration
     */
    private final int minNumber;
    private final int maxNumber;

    /**
     * constructor with args
     *
     * @param maxNumber - max number for random
     */
    public FloorsGenerator(final int maxNumber) {
        minNumber = 1;
        this.maxNumber = maxNumber;
    }

    /**
     * method which generate random pair of numbers
     *
     * @return array of 2 numbers(pair)
     */
    public int[] getPair() {
        int[] pair = new int[2];
        Random random = new Random();
        int a = random.nextInt(maxNumber) + minNumber;
        int b;
        b = random.nextInt(maxNumber) + minNumber;
        while (a == b) {
            b = random.nextInt(maxNumber) + minNumber;
        }
        pair[0] = a;
        pair[1] = b;
        return pair;
    }
}
