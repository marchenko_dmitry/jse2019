package by.epam.training.concurrency.model;

import by.epam.training.concurrency.enums.MovementDirection;
import by.epam.training.concurrency.enums.TransportationState;


/**
 * Class Passenger which initial passengers
 */
public class Passenger {
    /**
     * items declaration
     */
    private final int id;
    private final int sourceFloor;
    private final int destinationFloor;
    private TransportationState transportationState;
    private final MovementDirection movementDirection;

    /**
     * constructor with args
     *
     * @param sourceFloor      - source floor
     * @param destinationFloor - destination floor
     */
    public Passenger(final int sourceFloor, final int destinationFloor) {
        id = CreateId.getInstance().getNextId();
        this.sourceFloor = sourceFloor;
        this.destinationFloor = destinationFloor;
        this.transportationState = TransportationState.NOT_STARTED;
        if (sourceFloor < destinationFloor) {
            movementDirection = MovementDirection.UP;
        } else {
            movementDirection = MovementDirection.DOWN;
        }
    }

    /**
     * getter
     *
     * @return id of passenger
     */
    public int getId() {
        return id;
    }

    /**
     * getter
     *
     * @return source floor
     */
    public int getSourceFloor() {
        return sourceFloor;
    }

    /**
     * getter
     *
     * @return destination floor
     */
    public int getDestinationFloor() {
        return destinationFloor;
    }

    /**
     * getter
     *
     * @return element from transportation state
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * setter
     *
     * @param transportationState - state of transportation
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    /**
     * getter
     *
     * @return element from movement direction
     */
    public MovementDirection getMovementDirection() {
        return movementDirection;
    }

    /**
     * method which equal id
     *
     * @param o - some object
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Passenger passenger = (Passenger) o;
        return id == passenger.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    /**
     * private class for create new Id
     */
    private static final class CreateId {
        private static CreateId instance;
        private static int newId;

        /**
         * constructor with no args
         */
        private CreateId() {

        }

        /**
         * method which check instance is exists
         *
         * @return instance
         */
        static CreateId getInstance() {
            if (instance == null) {
                return new CreateId();
            }
            return instance;
        }

        /**
         * setter
         *
         * @param instance - instance
         */
        public static void setInstance(final CreateId instance) {
            CreateId.instance = instance;
        }

        /**
         * method which create new Id
         *
         * @return new id
         */
        int getNextId() {
            return newId++;
        }
    }

}
