package by.epam.training.concurrency.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Building which initial building
 */
public class Building {
    /**
     * items declaration
     */
    private final int floorNumb;
    private final List<Floor> floors;
    private final Elevator elevator;

    /**
     * constructor with args
     *
     * @param floorNumb        - number of floor
     * @param elevatorCapacity - capacity of elevator
     */
    public Building(final int floorNumb, final int elevatorCapacity) {
        this.floorNumb = floorNumb;
        floors = new ArrayList<>(floorNumb);
        initialFloors();
        this.elevator = new Elevator(floors, elevatorCapacity);
    }

    /**
     * getter
     *
     * @return floors
     */
    public List<Floor> getFloors() {
        return floors;
    }

    /**
     * getter
     *
     * @return elevator
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * method which initial floors
     */
    private void initialFloors() {
        for (int i = 1; i <= floorNumb; i++) {
            Floor floor = new Floor(i);
            floors.add(floor);
        }
    }

}
