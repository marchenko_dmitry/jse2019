package by.epam.training.concurrency.tasks;

import by.epam.training.concurrency.controllers.Controller;
import by.epam.training.concurrency.enums.TransportationState;
import by.epam.training.concurrency.model.Building;
import by.epam.training.concurrency.model.Elevator;
import by.epam.training.concurrency.model.Floor;
import by.epam.training.concurrency.model.Passenger;

/**
 * Class PassengerTransportationTask used like a passenger Thread
 */
public class PassengerTransportationTask implements Runnable {
    /**
     * item declaration
     */
    private final Passenger passenger;
    private final Controller controller;
    private final Elevator elevator;
    private final Floor sourceFloor;
    private final Floor destinationFloor;

    /**
     * constructor with args
     *
     * @param passenger  - passenger
     * @param building   - building
     * @param controller - controller of elevator
     */
    public PassengerTransportationTask(final Passenger passenger, final Building building,
                                       final Controller controller) {
        this.passenger = passenger;
        this.sourceFloor = building.getFloors().get(passenger.getSourceFloor() - 1);
        this.destinationFloor = building.getFloors().get(passenger.getDestinationFloor() - 1);
        this.elevator = building.getElevator();
        this.controller = controller;
    }

    /**
     * method which manage passenger thread
     */
    @Override
    public void run() {

        try {
            boolean completed = false;
            while (!completed) {
                sourceFloor.getLock().lock();
                try {
                    sourceFloor.getEnterCondition().await();
                    if (passenger.getMovementDirection() == elevator.getMovementDirection()) {
                        completed = controller.enter(passenger, elevator);
                        passenger.setTransportationState(TransportationState.IN_PROGRESS);
                    }
                    sourceFloor.getCompleted().signal();
                } finally {
                    sourceFloor.getLock().unlock();
                }
            }
            destinationFloor.getLock().lock();
            try {
                destinationFloor.getExitCondition().await();
                controller.exit(passenger, elevator);
                passenger.setTransportationState(TransportationState.COMPLETED);
                destinationFloor.getCompleted().signal();
            } finally {
                destinationFloor.getLock().unlock();
            }

        } catch (InterruptedException e) {
            passenger.setTransportationState(TransportationState.ABORTED);
        }
    }

}
