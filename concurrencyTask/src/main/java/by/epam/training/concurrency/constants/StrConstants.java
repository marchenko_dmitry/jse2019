package by.epam.training.concurrency.constants;

/**
 * Class with constants
 */
public final class StrConstants {
    public static final String FLOORS_NUMBER = "floorsNumber";
    public static final String ELEVATOR_CAPACITY = "elevatorCapacity";
    public static final String PASSENGERS_NUMBER = "passengersNumber";
    public static final String FROM = " from ";
    public static final String TO = " to ";
    public static final String WHITESPACE = " ";
    public static final String ON_STORY = " on the floor № ";
    public static final String CONFIG_FILE = "concurrencyTask/src/main/resources/config.properties";
}
