package by.epam.training.concurrency.enums;

/**
 * enum TransportationState which describe transportation state
 */
public enum TransportationState {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED,
    ABORTED
}
