package by.epam.training.concurrency.enums;

/**
 * enum MovementDirection which describe movement direction
 */
public enum MovementDirection {
    UP,
    DOWN,
}
