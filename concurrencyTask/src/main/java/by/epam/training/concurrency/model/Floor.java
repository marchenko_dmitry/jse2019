
package by.epam.training.concurrency.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class Floor which initial floors
 */
public class Floor {
    /**
     * items declaration
     */
    private final int number;
    private List<Passenger> arrivalContainer;
    private List<Passenger> dispatchContainer;
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition exitCondition = lock.newCondition();
    private final Condition enterCondition = lock.newCondition();
    private final Condition completed = lock.newCondition();

    /**
     * constructor with args
     *
     * @param number - number of floor
     */
    Floor(int number) {
        this.number = number;
        arrivalContainer = new ArrayList<>();
        dispatchContainer = new ArrayList<>();
    }

    /**
     * getter
     *
     * @return - number of floor
     */
    public int getNumber() {
        return number;
    }

    /**
     * getter
     *
     * @return - arrival container
     */
    public List<Passenger> getArrivalContainer() {
        return arrivalContainer;
    }

    /**
     * getter
     *
     * @return - dispatch container
     */
    public List<Passenger> getDispatchContainer() {
        return dispatchContainer;
    }

    /**
     * method which add arrival container
     *
     * @param passenger - passenger
     */
    public void addArrivalPassenger(final Passenger passenger) {
        arrivalContainer.add(passenger);
    }

    /**
     * method which add dispatch container
     *
     * @param passenger - passenger
     */
    public void addDispatchPassenger(final Passenger passenger) {
        dispatchContainer.add(passenger);
    }

    /**
     * method which remove dispatch container
     *
     * @param passenger - passenger
     */
    public void removeDispatchPassenger(final Passenger passenger) {
        dispatchContainer.remove(passenger);
    }

    /**
     * getter
     *
     * @return lock
     */
    public ReentrantLock getLock() {
        return lock;
    }

    /**
     * getter
     *
     * @return enter condition
     */
    public Condition getEnterCondition() {
        return enterCondition;
    }

    /**
     * getter
     *
     * @return exit condition
     */
    public Condition getExitCondition() {
        return exitCondition;
    }

    /**
     * getter
     *
     * @return completed
     */
    public Condition getCompleted() {
        return completed;
    }

}
