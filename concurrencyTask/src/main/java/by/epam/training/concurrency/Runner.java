package by.epam.training.concurrency;

import by.epam.training.concurrency.constants.StrConstants;
import by.epam.training.concurrency.controllers.Controller;
import by.epam.training.concurrency.generators.FloorsGenerator;
import by.epam.training.concurrency.model.Building;
import by.epam.training.concurrency.model.Passenger;
import by.epam.training.concurrency.readers.ReaderParameters;
import by.epam.training.concurrency.tasks.ElevatorMovementTask;
import by.epam.training.concurrency.tasks.PassengerTransportationTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;


public class Runner {

    public static void main(String[] args) throws InterruptedException {
        final String fileName = StrConstants.CONFIG_FILE;
        ReaderParameters reader = new ReaderParameters(fileName);
        final Building building = new Building(reader.getFloorNumber(), reader.getElevatorCapacity());
        final Controller controller = new Controller();
        List<PassengerTransportationTask> tasks = new ArrayList<>();
        FloorsGenerator generator = new FloorsGenerator(reader.getFloorNumber());

        for (int i = 0; i < reader.getNumberOfPassengers(); i++) {
            int[] pair = generator.getPair();
            int sourceFloor = pair[0];
            int destinationFloor = pair[1];
            Passenger passenger = new Passenger(sourceFloor, destinationFloor);
            PassengerTransportationTask transportationTask = new PassengerTransportationTask(passenger, building, controller);
            tasks.add(transportationTask);
            int floorIndex = sourceFloor - 1;
            building.getFloors().get(floorIndex).addDispatchPassenger(passenger);
        }


        ExecutorService executorService = Executors.newFixedThreadPool(reader.getNumberOfPassengers());
        for (PassengerTransportationTask transportationTask : tasks) {
            executorService.execute(transportationTask);
        }
        executorService.shutdown();

        ThreadPoolExecutor threadPoolExecutorElevator = new ThreadPoolExecutor(1, 1,
                0L, TimeUnit.SECONDS, new SynchronousQueue<>());
        threadPoolExecutorElevator.execute(new ElevatorMovementTask(building, reader.getNumberOfPassengers()));
        threadPoolExecutorElevator.shutdown();


    }

}