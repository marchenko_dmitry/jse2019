
package by.epam.training.concurrency.model;

import by.epam.training.concurrency.enums.MovementDirection;

import java.util.ArrayList;

import java.util.List;

/**
 * Class Elevator which initial elevator
 */
public class Elevator {
    /**
     * items declaration
     */
    private final List<Floor> floors;
    private final int capacity;
    private int curFloorIndex;
    private Floor curFloor;
    private List<Passenger> passengersContainer;
    private MovementDirection movementDirection;

    /**
     * constructor with args
     *
     * @param floors   - list of floors
     * @param capacity - capacity
     */
    public Elevator(final List<Floor> floors, final int capacity) {
        this.floors = floors;
        this.capacity = capacity;
        this.curFloorIndex = 0;
        this.curFloor = floors.get(curFloorIndex);
        this.passengersContainer = new ArrayList<>(capacity);
        this.movementDirection = MovementDirection.UP;
    }

    /**
     * getter
     *
     * @return container of passengers
     */
    public List<Passenger> getPassengersContainer() {
        return passengersContainer;
    }

    /**
     * getter
     *
     * @return current floor
     */
    public Floor getCurFloor() {
        return curFloor;
    }

    /**
     * getter
     *
     * @return movement direction
     */
    public MovementDirection getMovementDirection() {
        return movementDirection;
    }

    /**
     * getter
     *
     * @return index of current floor
     */
    public int getCurFloorIndex() {
        return curFloorIndex;
    }

    /**
     * getter
     *
     * @return capacity of elevator
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * method which add passenger to container of passengers
     *
     * @param passenger - passenger
     * @return true if add, false if container is full
     */
    public boolean addPassenger(final Passenger passenger) {
        if (isFull()) return false;
        passengersContainer.add(passenger);
        return true;
    }

    /**
     * method which remove passenger from container of passengers
     *
     * @param passenger - passenger
     */
    public void removePassenger(final Passenger passenger) {
        passengersContainer.remove(passenger);
    }

    /**
     * method which move elevator to next floor
     */
    public void moveElevator() {

        if (movementDirection == MovementDirection.UP) {
            nextFloor();
        } else {
            previousFloor();
        }
        checkLastFloor();
    }

    /**
     * method which check floors on last
     */
    private void checkLastFloor() {
        final int firstFloorIndex = 0;
        final int lastFloorIndex = floors.size() - 1;
        if (curFloorIndex == firstFloorIndex) {
            movementDirection = MovementDirection.UP;
        }
        if (curFloorIndex == lastFloorIndex) {
            movementDirection = MovementDirection.DOWN;
        }
    }

    /**
     * method which check container of passengers is full
     *
     * @return true if full, false if not
     */
    private boolean isFull() {
        return passengersContainer.size() == capacity;
    }

    /**
     * method which move elevator in next floor
     */
    private void nextFloor() {
        curFloorIndex++;
        curFloor = floors.get(curFloorIndex);
    }

    /**
     * method which move elevator in previous floor
     */
    private void previousFloor() {
        curFloorIndex--;
        curFloor = floors.get(curFloorIndex);
    }


}
