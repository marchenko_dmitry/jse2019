package by.epam.training.concurrency.validators;

import by.epam.training.concurrency.enums.TransportationState;
import by.epam.training.concurrency.model.Building;
import by.epam.training.concurrency.model.Floor;
import by.epam.training.concurrency.model.Passenger;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Class Validator check all passengers on their places
 */
public class Validator {
    /**
     * initial elements for checking conditions
     */
    private final Building building;
    private final List<Floor> floors;
    private final int passengersNumber;
    /**
     * logger
     */
    private static final Logger logger = Logger.getLogger(Validator.class);


    /**
     * constructor with args
     *
     * @param building         - building
     * @param passengersNumber - number of passenger
     */
    public Validator(Building building, int passengersNumber) {
        this.building = building;
        floors = building.getFloors();
        this.passengersNumber = passengersNumber;
    }

    /**
     * method which check for validate transportation
     */
    public void validateTransportation() {
        boolean valid = this.isElevatorEmpty() && this.checkConditions();
        if (valid) {
            Validator.logger.info("Transportation of passengers is VALID");
        } else {
            Validator.logger.info("Transportation of passengers is INVALID");
        }
    }

    /**
     * method which check conditions
     *
     * @return true if all passenger went to destination floor, else if someone no
     */
    public boolean checkConditions() {
        int passengersCounter = 0;
        for (final Floor floor : this.floors) {
            final List<Passenger> arrival = floor.getArrivalContainer();
            final List<Passenger> dispatch = floor.getDispatchContainer();
            if (!dispatch.isEmpty()) {
                Validator.logger.info("Dispatch container on the floor " + floor.getNumber() + " not empty");
                return false;
            }
            for (final Passenger passenger : arrival) {
                passengersCounter++;
                if (!(TransportationState.COMPLETED == passenger.getTransportationState())) {
                    Validator.logger.info("Passenger with id = " + passenger.getId() + " transportation state is "
                            + passenger.getTransportationState() + " instead of COMPLETED");
                    return false;
                }
                if (passenger.getDestinationFloor() != floor.getNumber()) {
                    Validator.logger.info("Passenger with id = " + passenger.getId() + " destination floor is "
                            + passenger.getDestinationFloor() + " instead of " + floor.getNumber());
                    return false;
                }
            }
        }
        return passengersCounter == this.passengersNumber;
    }

    /**
     * method which check elevator is empty
     *
     * @return true if empty, false if not
     */
    public boolean isElevatorEmpty() {
        return building.getElevator().getPassengersContainer().isEmpty();
    }
}
