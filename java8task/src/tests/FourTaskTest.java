package tests;


import by.epam.training.task4.Author;
import by.epam.training.task4.Book;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class FourTaskTest {
    private static final Author[] authors = new Author[]{
            new Author("Bulgakov", (short) 32, new ArrayList<>()),
            new Author("Gray", (short) 32, new ArrayList<>()),
            new Author("Mayakovski", (short) 32, new ArrayList<>()),
            new Author("London", (short) 32, new ArrayList<>()),

    };

    private static final Book[] books = new Book[]{
            new Book("master&margo", 150, Arrays.asList(authors).subList(0, 1)),
            new Book("viy", 250, Arrays.asList(authors).subList(1, 2)),
            new Book("1962", 100, Arrays.asList(authors).subList(2, 3)),
            new Book("pink sky", 300, Arrays.asList(authors).subList(3, 4)),
            new Book("biggest", 500, Arrays.asList(authors).subList(1, 2)),
    };

    static {
        authors[0].setBooks(Collections.singletonList(books[0]));
        authors[1].setBooks(List.of(books[1], books[4]));
        authors[2].setBooks(Collections.singletonList(books[2]));
        authors[3].setBooks(Collections.singletonList(books[3]));
    }


    @Test
    public void moreThanTwoHundredPagesTest() {
        assertFalse(Arrays.stream(books).allMatch(x -> x.getNumberOfPages() > 200));
    }

    @Test
    public void bookWithMaxPagesTest() {
        assertEquals(books[4], Arrays.stream(books).max(Comparator.comparing(Book::getNumberOfPages)).orElse(null));
    }

    @Test
    public void bookWithMinPagesTest() {
        assertEquals(books[2], Arrays.stream(books).min(Comparator.comparing(Book::getNumberOfPages)).orElse(null));
    }

    @Test
    public void booksWithSingleAuthorTest() {
        assertEquals(Arrays.asList(books[0], books[1], books[2], books[3], books[4]),
                Arrays.stream(books)
                        .filter(x -> x.getAuthors().size() == 1)
                        .collect(Collectors.toList()));
    }

    @Test
    public void sortedByNumberOfPagesTest() {
        assertEquals(Arrays.asList(books[2], books[0], books[1], books[3], books[4]),
                Arrays.stream(books)
                        .sorted(Comparator.comparing(Book::getNumberOfPages))
                        .collect(Collectors.toList()));
    }

    @Test
    public void allTitlesTest() {
        assertEquals(Arrays.asList(books[0].getTitle(), books[1].getTitle(), books[2].getTitle(), books[3].getTitle(),
                books[4].getTitle()),
                Arrays.stream(books).map(Book::getTitle).collect(Collectors.toList()));
    }

    @Test
    public void distinctListOfAuthorsTest() {
        assertEquals(Arrays.asList(authors[0], authors[1], authors[2], authors[3]),
                Arrays.stream(books)
                        .flatMap(x -> x.getAuthors().stream())
                        .distinct()
                        .collect(Collectors.toList()));
    }

    @Test
    public void biggestBookTest() {
        assertEquals(books[4], Arrays.stream(authors).filter(x -> x.getName().equals("Gray"))
                .flatMap(x -> x.getBooks().stream())
                .max(Comparator.comparing(Book::getNumberOfPages)).orElse(null));
    }
}
