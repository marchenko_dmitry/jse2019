
import by.epam.training.task1.Person;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;


public class FirstTaskTest {
    private static final List<Person> persons = Arrays.asList(
            new Person("Dima", 13),
            new Person("Ivan", 32),
            new Person("Roma", 12),
            new Person("Egor", 21)
    );

    @Test
    public final void orderingByNameTest() {
        List<Person> sortedPersons = persons.stream()
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());

        assertEquals(persons.get(0), sortedPersons.get(0));
        assertEquals(persons.get(3), sortedPersons.get(1));
        assertEquals(persons.get(1), sortedPersons.get(2));
        assertEquals(persons.get(2), sortedPersons.get(3));
    }

    @Test
    public final void orderingByAgeTest() {
        List<Person> sortedPersons = persons.stream()
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());

        assertEquals(persons.get(2), sortedPersons.get(0));
        assertEquals(persons.get(0), sortedPersons.get(1));
        assertEquals(persons.get(3), sortedPersons.get(2));
        assertEquals(persons.get(1), sortedPersons.get(3));
    }
}