package by.epam.training.task3;


import org.apache.log4j.Logger;

public class Runner3 {

    public static final Logger logger = Logger.getLogger(Runner3.class.getName());
    private static ThreeFunction<Integer, Integer, Integer> sumThreeFuntion = (integer, integer2, integer3) -> integer + integer2 + integer3;
    private static ThreeFunction<Integer, Integer, Integer> diffThreeFuntion = (integer, integer2, integer3) -> integer - integer2 - integer3;

    public static void main(String[] args) {

        logger.info(String.valueOf(sumThreeFuntion.result(1, 2, 3)));
        logger.info(String.valueOf(diffThreeFuntion.result(3, 5, 1)));
    }

}
