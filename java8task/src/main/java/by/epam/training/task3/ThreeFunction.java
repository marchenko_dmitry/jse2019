package by.epam.training.task3;

@FunctionalInterface
public interface ThreeFunction<T, P, R> {
    /**
     * function takes 3 args and return result.
     *
     * @param t  - input param
     * @param p - input param
     * @param r - input param
     * @return result
     */
    R result(T t, P p, R r);


}
