package by.epam.training.task4;

import java.util.List;
import java.util.stream.Collectors;

public class Author {
    /**
     * name declaration.
     */
    private String name;
    /**
     * age declaration.
     */
    private short age;
    /**
     * books declaration.
     */
    private List<Book> books;

    /**
     * constructor with args.
     *
     * @param name  - name of author
     * @param age   - age of author
     * @param books - books
     */
    public Author(final String name, final short age, final List<Book> books) {
        this.name = name;
        this.age = age;
        this.books = books;
    }

    /**
     * getter.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * getter.
     *
     * @return books
     */

    public List<Book> getBooks() {
        return books;
    }

    /**
     * setter.
     *
     * @param books - list of books
     */
    public void setBooks(final List<Book> books) {
        this.books = books;
    }

    /**
     * method produce class author in string format.
     *
     * @return string value
     */
    @Override
    public String toString() {
        return name + " " + age + " " + books.stream().map(Book::getTitle)
                .collect(Collectors.joining(", "));
    }
}
