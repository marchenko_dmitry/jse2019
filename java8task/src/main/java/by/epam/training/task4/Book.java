package by.epam.training.task4;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class Book.
 */
public class Book {
    /**
     * title declaration.
     */
    private final String title;
    /**
     * authors declaration.
     */
    private final List<Author> authors;
    /**
     * nubmer of pages declaration.
     */
    private final int numberOfPages;

    /**
     * constructor with args.
     *
     * @param title         - title of book
     * @param numberOfPages - number of pages
     * @param authors       - authors
     */
    public Book(final String title, final int numberOfPages,
                final List<Author> authors) {
        this.title = title;
        this.authors = authors;
        this.numberOfPages = numberOfPages;
    }

    /**
     * getter.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * getter.
     *
     * @return authors
     */
    public List<Author> getAuthors() {
        return authors;
    }

    /**
     * getter.
     *
     * @return nubmer of pages
     */
    public int getNumberOfPages() {
        return numberOfPages;
    }

    /**
     * method which equal objects for some parameters.
     *
     * @param o - object
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Book book = (Book) o;
        return numberOfPages == book.numberOfPages
                && title.equals(book.title)
                && authors.equals(book.authors);
    }

    /**
     * method which create hashcode.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(title, authors, numberOfPages);
    }

    /**
     * method which produce class book in string format.
     *
     * @return string value
     */
    @Override
    public String toString() {
        return getTitle() + " " + authors.stream().map(Author::getName)
                .collect(Collectors.joining(", "))
                + " " + getNumberOfPages();
    }
}
