package by.epam.training.task4;


import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner4 {
    /**
     * logger
     */
    private static final Logger logger = Logger.getLogger(Runner4.class.getName());


    /**
     * method with subtask3
     *
     * @param books      - books
     * @param isParallel - true, use parallel stream, false - not
     */
    private static void subtask3(Book[] books, boolean isParallel) {
        logger.info("check if some/all book(s) have more than 200 pages " +
                getStreamBook(books, isParallel).allMatch(book -> book.getNumberOfPages() > 200));

        logger.info("find the books with max and min number of pages");
        Optional<Book> bookWithMaxNumberOfPages =
                getStreamBook(books, isParallel)
                        .max(Comparator.comparingInt(Book::getNumberOfPages));
        Optional<Book> bookWithMinNumberOfPages = getStreamBook(books, isParallel)
                .min(Comparator.comparingInt(Book::getNumberOfPages));
        bookWithMaxNumberOfPages.ifPresent(book ->
                logger.info("book with max number of pages is "
                        + book.getTitle() + " " + book.getAuthors()
                        + " " + book.getNumberOfPages()));
        bookWithMinNumberOfPages.ifPresent(book ->
                logger.info("book with max number of pages is " + book.getTitle()
                        + " " + book.getAuthors()
                        + " " + book.getNumberOfPages()));

        logger.info("filter books with only single author");
        List<Book> booksWithSingleAuthor = getStreamBook(books, isParallel)
                .filter(book -> book.getAuthors().size() == 1)
                .collect(Collectors.toList());
        booksWithSingleAuthor.forEach(book -> logger.info(book.getTitle() + " " + book.getAuthors()
                + " " + book.getNumberOfPages()));

        logger.info("sort the books by number of pages/title");
        List<Book> sortedBook = getStreamBook(books, isParallel)
                .sorted(Comparator.comparing(Book::getNumberOfPages))
                .collect(Collectors.toList());
        logger.info(sortedBook);

        logger.info("get list of all titles");
        getStreamBook(books, isParallel).map(Book::getTitle)
                .collect(Collectors.toList()).forEach(logger::info);

        logger.info("get distinct list of all authors");
        List<Author> authorList = getStreamBook(books, isParallel).peek(logger::info)
                .flatMap(x -> x.getAuthors().stream())
                .distinct()
                .collect(Collectors.toList());
        logger.info(authorList);


    }

    /**
     * method for getting stream book
     *
     * @param books      books
     * @param isParallel - true, use parallel stream, false - not
     * @return stream
     */
    private static Stream<Book> getStreamBook(Book[] books, boolean isParallel) {
        Supplier<Stream<Book>> streamSupplier = () -> Arrays.stream(books);
        Supplier<Stream<Book>> parallelSupplier = () -> Arrays.stream(books).parallel();
        if (isParallel) {
            return parallelSupplier.get();
        } else {
            return streamSupplier.get();
        }
    }


    public static void main(String[] args) {
        Author[] authors = new Author[]{
                new Author("Bulgakov", (short) 32, new ArrayList<>()),
                new Author("Gray", (short) 32, new ArrayList<>()),
                new Author("Mayakovski", (short) 32, new ArrayList<>()),
                new Author("London", (short) 32, new ArrayList<>()),

        };

        Book[] books = new Book[]{
                new Book("master&margo", 150, Arrays.asList(authors).subList(0, 1)),
                new Book("viy", 250, Arrays.asList(authors).subList(1, 2)),
                new Book("1962", 100, Arrays.asList(authors).subList(2, 3)),
                new Book("pink sky", 300, Arrays.asList(authors).subList(3, 4)),
                new Book("bigest", 500, Arrays.asList(authors).subList(1, 2)),
        };
        authors[0].setBooks(Collections.singletonList(books[0]));
        authors[1].setBooks(List.of(books[1], books[4]));
        authors[2].setBooks(Collections.singletonList(books[2]));
        authors[3].setBooks(Collections.singletonList(books[3]));

        logger.info("initial end");

        subtask3(books, false);

        logger.info("subtask3 with parallel stream");
        subtask3(books, true);

        Optional<Book> grayBook = Arrays.stream(authors)
                .filter(author -> author.getName().equals("Gray"))
                .flatMap(author -> author.getBooks().stream())
                .max(Comparator.comparingInt(Book::getNumberOfPages));
        logger.info("the biggest gray's book is ");
        grayBook.ifPresent(logger::info);

    }
}
