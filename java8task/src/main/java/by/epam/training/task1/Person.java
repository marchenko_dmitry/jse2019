package by.epam.training.task1;

import java.util.Objects;

/**
 * Class Person.
 */
public class Person {

    /**
     * item declaration name.
     */
    private String name;
    /**
     * item declaration age.
     */
    private Integer age;

    /**
     * constructor with args.
     *
     * @param nameInput - name of person
     * @param ageInput  - age of person
     */
    public Person(final String nameInput, final Integer ageInput) {
        this.name = nameInput;
        this.age = ageInput;
    }

    /**
     * getter.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter.
     *
     * @param nameInput - name of person
     */
    public void setName(final String nameInput) {
        this.name = nameInput;
    }

    /**
     * getter.
     *
     * @return age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * setter.
     *
     * @param ageInput - age of person
     */
    public void setAge(final Integer ageInput) {
        this.age = ageInput;
    }

    /**
     * method which equal elements.
     *
     * @param o - any object
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return age.equals(person.age)
                && Objects.equals(name, person.name);
    }

    /**
     * method which find hash code.
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    /**
     * method which produce class person to string format.
     *
     * @return string element
     */
    @Override
    public String toString() {
        return name + "; " + age;
    }

}
