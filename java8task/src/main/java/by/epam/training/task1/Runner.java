package by.epam.training.task1;


import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class Runner.
 */
public class Runner {
    /**
     * logger
     */
    private static final Logger logger = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Dima", 12));
        people.add(new Person("Vlad", 15));
        people.add(new Person("Kolya", 13));
        people.add(new Person("Ivan", 32));

        Comparator<Person> comparatorName =
                (o1, o2) -> o1.getName().compareTo(o2.getName());
        Comparator<Person> comparatorAge =
                (o1, o2) -> o1.getAge().compareTo(o2.getAge());

        people.stream().sorted(comparatorName).forEach(logger::info);
        people.stream().sorted(comparatorAge).forEach(logger::info);


    }


}
