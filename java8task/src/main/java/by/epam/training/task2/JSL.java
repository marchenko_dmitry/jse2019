package by.epam.training.task2;


@FunctionalInterface
public interface JSL {

    /**
     * method write that interface is using.
     * @param a - any int element
     * @param b - any int element
     * @return multiply a & b
     */
    default int mul(final int a, final int b) {
        return a * b;
    }

    /**
     * methd find sum.
     *
     * @param a - int input parameter
     * @param b - int input parameter
     * @return result
     */
    int sum(int a, int b);

    /**
     * method find abs of some int number.
     *
     * @param a - input param
     * @return - abs of a
     */
    default int abs(final int a) {
        return Math.abs(a);
    }

    /**
     * convert int value in string and print it.
     * @param a - any int value
     * @return a in string format
     */
    static String converter(final int a) {
        return String.valueOf(a);
    }


}
