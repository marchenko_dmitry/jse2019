package by.epam.training.task2;


import org.apache.log4j.Logger;

import java.util.function.*;

public class Runner2 {
    private static final Logger logger = Logger.getLogger(Runner2.class.getName());
    public static void main(String[] args) {
        /**
         * predicate
         */
        Predicate<Integer> isNegative = x -> x < 0;
        logger.info(isNegative.test(-3));
        logger.info(isNegative.test(5));

        /**
         * binaryOperator
         */
        BinaryOperator<Integer> multiply = (a, b) -> a * b;
        logger.info(multiply.apply(3, 5));
        logger.info(multiply.apply(-1, 6));

        /**
         * unaryOperator
         */
        UnaryOperator<Integer> square = arg -> arg * arg;
        logger.info(square.apply(4));
        logger.info(square.apply(5));

        /**
         * Function
         */
        Function<Integer, String> convert = rubs -> String.valueOf(rubs) + " rubs";
        logger.info(convert.apply(10));
        logger.info(convert.apply(2));

        /**
         * consumer
         */
        Consumer<Integer> printer = integer ->  logger.info(integer + "people");
        printer.accept(10);
        printer.accept(15);

        /**
         * supplier
         */
        Supplier<Integer> supplier = () -> 5;
        logger.info(supplier.get());

        JSL jsl = (a, b) -> a + b;
        jsl.mul(2,3);
        jsl.sum(3, 5);
        logger.info(jsl.abs(-3));
        logger.info(JSL.converter(5));


    }


}
