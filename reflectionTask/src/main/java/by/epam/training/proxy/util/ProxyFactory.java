package by.epam.training.proxy.util;

import by.epam.training.proxy.annotations.Proxy;
import by.epam.training.proxy.exceptions.InstantiationFactoryException;

import java.lang.reflect.InvocationHandler;
import java.text.MessageFormat;


public class ProxyFactory {

    public static <T> T getInstanceOf(final Class<? extends T> aClass, final Class<T> bClass) {
        String invocationHandlerName = null;
        try {
            T instance = aClass.getDeclaredConstructor().newInstance();
            boolean isProxy = aClass.isAnnotationPresent(Proxy.class);
            if (isProxy) {
                Proxy proxy = aClass.getAnnotation(Proxy.class);
                invocationHandlerName = proxy.invocationHandler();
                Class<?> invocationHandlerClass = Class.forName(invocationHandlerName);
                InvocationHandler invocationHandler = (InvocationHandler) invocationHandlerClass
                        .getConstructor(bClass).newInstance(instance);
                instance = bClass.cast(java.lang.reflect.Proxy.newProxyInstance(
                        aClass.getClassLoader(), new Class[]{bClass}, invocationHandler));
            }
            return instance;
        } catch (ClassNotFoundException e) {
            throw new InstantiationFactoryException("can't find invocation handler class: ", e);
        } catch (NoSuchMethodException e) {
            throw new InstantiationFactoryException(MessageFormat.format(
                    "constructor of handler class doesn't take object",
                    invocationHandlerName, bClass), e);
        } catch (ReflectiveOperationException e) {
            throw new InstantiationFactoryException("Failed to instantiate class: " + aClass, e);
        }
    }

}
