package by.epam.training.proxy.exceptions;

public class InstantiationFactoryException extends RuntimeException {

    /**
     * Constructs a new runtime exception with
     * the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   the cause
     */
    public InstantiationFactoryException(final String message,
                                         final Throwable cause) {
        super(message, cause);
    }


}
