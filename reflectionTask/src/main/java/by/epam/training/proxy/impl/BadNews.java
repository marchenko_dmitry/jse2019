package by.epam.training.proxy.impl;

import by.epam.training.proxy.interfaces.News;

public class BadNews implements News {

    private static final String BAD_MESSAGE = "Something is bad ";

    @Override
    public String getNews(final int times) {
        StringBuilder result;
        result = new StringBuilder();
        result.append(BAD_MESSAGE.repeat(Math.max(0, times)));
        return String.valueOf(result);
    }
}
