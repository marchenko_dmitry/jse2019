package by.epam.training.proxy.interfaces;

/**
 * Interface for getting news
 */
public interface News {
    /**
     * getting news
     *
     * @return kind of news
     */
    String getNews(final int times);
}
