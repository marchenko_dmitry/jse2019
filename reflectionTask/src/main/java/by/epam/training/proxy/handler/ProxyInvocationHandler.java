package by.epam.training.proxy.handler;

import by.epam.training.proxy.interfaces.News;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ProxyInvocationHandler implements InvocationHandler {
    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(ProxyInvocationHandler.class);
    /**
     * instance of proxied class for invocation handling
     */
    private final News news;


    /**
     * constructor with args
     *
     * @param news - obj for proxy wrapper
     */
    public ProxyInvocationHandler(final News news) {
        this.news = news;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        logger.info("name = " + method.getName());
        logger.info("arguments :");
        Arrays.stream(args).forEach(logger::info);
        logger.info("parameters :");
        Arrays.stream(method.getParameters()).forEach(logger::info);
        return method.invoke(news, args);
    }
}
