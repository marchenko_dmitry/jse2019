package by.epam.training.proxy;

import by.epam.training.proxy.impl.BadNews;
import by.epam.training.proxy.impl.GoodNews;
import by.epam.training.proxy.interfaces.News;
import by.epam.training.proxy.util.ProxyFactory;
import org.apache.log4j.Logger;

/**
 * RunnerEqual class
 */
public class Runner {
    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {
        News badNews = ProxyFactory.getInstanceOf(BadNews.class, News.class);
        News goodNews = ProxyFactory.getInstanceOf(GoodNews.class, News.class);

        logger.info("Bad news message: " + badNews.getNews(2));

        logger.info("Good news message: " + goodNews.getNews(2));


    }
}
