package by.epam.training.proxy.impl;

import by.epam.training.proxy.annotations.Proxy;
import by.epam.training.proxy.interfaces.News;

@Proxy(invocationHandler = "by.epam.training.proxy.handler.ProxyInvocationHandler")
public class GoodNews implements News {

    private static final String GOOD_MESSAGE = "Everything is good ";

    @Override
    public String getNews(final int times) {
        StringBuilder result;
        result = new StringBuilder();
        result.append(GOOD_MESSAGE.repeat(Math.max(0, times)));
        return String.valueOf(result);
    }
}
