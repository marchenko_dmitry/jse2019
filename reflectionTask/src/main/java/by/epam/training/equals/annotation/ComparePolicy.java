package by.epam.training.equals.annotation;

public enum ComparePolicy {
    EQUAL, REFERENCE
}
