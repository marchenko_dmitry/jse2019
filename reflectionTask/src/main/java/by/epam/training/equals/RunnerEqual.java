package by.epam.training.equals;

import by.epam.training.equals.bean.Book;
import by.epam.training.equals.util.EqualityAnalyzer;
import org.apache.log4j.Logger;


public class RunnerEqual {
    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(RunnerEqual.class.getName());

    /**
     * Main method - entry point of the project.
     * @param args arguments (not used)
     */
    public static void main(final String[] args) {
        Book masterAndMargarita = new Book("Master & Margarita",
                "Bulgakov", 1875, 700);
        Book masterAndMargaritaDraft = new Book("Master & Margarita",
                "Bulgakov", 1875, 600);
        Book blackCat = new Book("Black cat", "Bulgakov", 1876, 700);

        logger.info("masterAndMargarita and masterAndMargaritaDraft equality is "
                + EqualityAnalyzer.equalObjects(masterAndMargarita, masterAndMargaritaDraft));
        logger.info("masterAndMargarita and blackCat equality is "
                + EqualityAnalyzer.equalObjects(masterAndMargarita, blackCat));

    }
}
