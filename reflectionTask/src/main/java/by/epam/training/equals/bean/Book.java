package by.epam.training.equals.bean;


import by.epam.training.equals.annotation.ComparePolicy;
import by.epam.training.equals.annotation.Equal;

public class Book {

    /**
     * Name of the book.
     */
    @Equal(compareBy = ComparePolicy.REFERENCE)
    public String name;

    /**
     * Author of the book.
     */
    @Equal(compareBy = ComparePolicy.REFERENCE)
    public String author;

    /**
     * Year of publication.
     */
    @Equal(compareBy = ComparePolicy.EQUAL)
    public int year;

    private int numbOfPages;


    /**
     * Default constructor.
     */
    public Book() {
    }


    /**
     * Constructor with parameters.
     *
     * @param name        name of the book
     * @param author      author of the book
     * @param year        year of publication
     * @param numbOfPages number of pages
     */
    public Book(String name, String author, int year, int numbOfPages) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.numbOfPages = numbOfPages;
    }

    /**
     * Name getter.
     *
     * @return name of the book
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter.
     *
     * @param name name of the book
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Author getter.
     *
     * @return author of the book
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Author setter.
     *
     * @param author author of the book
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * Year getter.
     *
     * @return year of publication
     */
    public int getYear() {
        return year;
    }

    /**
     * Year setter.
     *
     * @param year year of publication
     */
    public void setYear(final int year) {
        this.year = year;
    }

    public int getNumbOfPages() {
        return numbOfPages;
    }

    public void setNumbOfPages(int numbOfPages) {
        this.numbOfPages = numbOfPages;
    }


}
