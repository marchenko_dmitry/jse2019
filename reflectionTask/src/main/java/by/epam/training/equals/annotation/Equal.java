package by.epam.training.equals.annotation;

import java.lang.annotation.*;

/**
 * Annotation Equals
 *
 * @author Dmitry Marchenko
 */
@Inherited
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Equal {
    /**
     * return compare policy
     * @return compare policy
     */
    ComparePolicy compareBy();
}
