package by.epam.training.equals.util;

import by.epam.training.equals.annotation.Equal;
import by.epam.training.equals.exceptions.EqualityException;
import org.apache.commons.lang3.ClassUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * class for comparing
 */
public final class EqualityAnalyzer {
    /**
     * Private empty constructor
     */
    private EqualityAnalyzer() {
    }

    /**
     * Comparing two objects*
     *
     * @param objA the first object for compare
     * @param objB the second object for compare
     * @return true if objects are equal, false if not
     */
    public static boolean equalObjects(final Object objA, final Object objB) {
        if (objA == objB) {
            return true;
        }
        if (Objects.isNull(objA) || Objects.isNull(objB)) {
            return false;
        }
        if (!isExistCommonParent(objA.getClass(), objB.getClass())) {
            return false;
        }

        Set<Field> aField = getAnnotatedFields(objA.getClass());
        Set<Field> bField = getAnnotatedFields(objB.getClass());

        if (!aField.equals(bField)) {
            return false;
        }

        return equalFields(aField, objA, objB);
    }

    /**
     * check is classes have common parent class
     *
     * @param aClass the first class for checking
     * @param bClass the second class for checking
     * @return true if classes have common parent and false if not
     */
    public static boolean isExistCommonParent(final Class<?> aClass, final Class<?> bClass) {
        Class<?> utilClass = aClass;
        while (!utilClass.isAssignableFrom(bClass)) {
            utilClass = utilClass.getSuperclass();
            if (utilClass.isInterface() || utilClass.equals(Object.class)) {
                return false;
            }
        }
        return true;
    }

    /**
     * getting list of annotated  fields up the hierarchy
     *
     * @param aClass class from which need start to get fields
     * @return set of annotated fields up the hierarchy.
     */
    public static Set<Field> getAnnotatedFields(final Class<?> aClass) {
        Set<Field> annotatedFields = new HashSet<>();
        List<Field> fields;
        fields = getFieldsUpTo(aClass, Object.class);

        for (Field field : fields) {
            if (field.isAnnotationPresent(Equal.class)) {
                annotatedFields.add(field);
            }
        }

        return annotatedFields;
    }

    /**
     * getting list of all fields up the hierarchy of classes to exclusive parent.
     *
     * @param aClass          the class from which need start to get fields
     * @param exclusiveParent the class up to which need continue to get fields
     * @return list of fields up the hierarchy.
     */
    public static List<Field> getFieldsUpTo(final Class<?> aClass, final Class<?> exclusiveParent) {
        List<Field> currentClassFields = new ArrayList<>(Arrays.asList(aClass.getDeclaredFields()));
        Class<?> parentClass = aClass.getSuperclass();

        if (Objects.nonNull(parentClass) && !parentClass.equals(exclusiveParent)) {
            List<Field> parentClassFields = getFieldsUpTo(parentClass, exclusiveParent);
            currentClassFields.addAll(parentClassFields);
        }

        return currentClassFields;
    }

    /**
     * Equal objects by all annotated fields.
     *
     * @param annotatedFields the set of annotated objects fields for comparing
     * @param objA            the first object for compare
     * @param objB            the second object for compare
     * @return The {@code true} if objects references are equal and {@code false} otherwise.
     */
    public static boolean equalFields(final Set<Field> annotatedFields, final Object objA, final Object objB) {
//
        boolean flag = false;
        if (!objA.getClass().equals(objB.getClass())) {
            return false;
        }
        List<Field> fieldObjA = Arrays.stream(objA.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Equal.class))
                .collect(Collectors.toList());
        List<Field> fieldObjB = Arrays.stream(objB.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Equal.class))
                .collect(Collectors.toList());
        if (!fieldObjA.equals(fieldObjB)) {
            return false;
        }
        try {
            for (Field field : fieldObjA) {
                flag = field.get(objA).equals(field.get(objB));
            }

        } catch (IllegalAccessException e) {
            throw new EqualityException(e.getMessage(), e);
        }
        return flag;
    }


    /**
     * Equal objects by reference (but no matter what primitive wrapper classes equal by value).
     *
     * @param objA the first object for compare
     * @param objB the second object for compare
     * @return The {@code true} if objects references are equal and {@code false} otherwise.
     */
    public static boolean equalByReference(final Object objA, final Object objB) {
        boolean isPrimitive = ClassUtils.isPrimitiveWrapper(objA.getClass())
                && ClassUtils.isPrimitiveWrapper(objB.getClass());

        boolean isStrings = objA instanceof String && objB instanceof String;

        if (isPrimitive || isStrings) {
            return equalByValue(objA, objB);
        }
        return objA == objB;
    }

    /**
     * Equal objects by value.
     *
     * @param objA the first object for compare
     * @param objB the second object for compare
     * @return true  if objects values are equal and false if not
     */
    public static boolean equalByValue(final Object objA, final Object objB) {
        if (objA == objB) {
            return true;
        }
        return !Objects.isNull(objA) && objA.equals(objB);
    }
}
