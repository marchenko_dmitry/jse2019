package by.epam.training.equals.exceptions;

public class EqualityException extends RuntimeException {
    /**
     * Constructs a new runtime exception
     *
     * @param message the detail message
     * @param cause   the cause
     */
    public EqualityException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
