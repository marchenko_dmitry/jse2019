package by.epam.training.controller;

import by.epam.training.dto.CategoryDto;
import by.epam.training.facade.CategoryFacade;
import by.epam.training.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Category controller.
 */
@RestController
@RequestMapping("/category")
public class CategoryController extends Controller<Category, CategoryDto> {

    private final CategoryFacade categoryFacade;

    /**
     * Create category dto.
     *
     * @param categoryDto the category dto
     * @return the category dto
     */
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public CategoryDto create(@RequestBody final @Valid CategoryDto categoryDto) {
        return categoryFacade.create(categoryDto);
    }

    /**
     * Search list.
     *
     * @param name     the name
     * @param pageable the pageable
     * @return the list
     */
    @GetMapping("/name/{name}")
    public List<CategoryDto> search(@PathVariable("name") final String name, final Pageable pageable) {
        return categoryFacade.searchAll(pageable, name);
    }

    /**
     * Instantiates a new Category controller.
     *
     * @param categoryFacade the category facade
     */
    @Autowired
    public CategoryController(final CategoryFacade categoryFacade) {
        super(categoryFacade);
        this.categoryFacade = categoryFacade;
    }


}
