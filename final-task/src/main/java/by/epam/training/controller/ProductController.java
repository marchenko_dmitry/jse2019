package by.epam.training.controller;

import by.epam.training.dto.PriceDto;
import by.epam.training.dto.ProductCreationDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.facade.ProductFacade;
import by.epam.training.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Product controller.
 */
@RestController
@RequestMapping("/product")
public class ProductController extends Controller<Product, ProductDto> {

    /**
     * The constant PRODUCT_ID.
     */
    public static final String PRODUCT_ID = "productId";
    /**
     * The constant CATEGORY_ID.
     */
    public static final String CATEGORY_ID = "categoryId";
    /**
     * The constant PRICE_ID.
     */
    public static final String PRICE_ID = "priceId";
    private final ProductFacade facade;

    /**
     * Instantiates a new Product controller.
     *
     * @param facade the facade
     */
    @Autowired
    public ProductController(ProductFacade facade) {
        super(facade);
        this.facade = facade;
    }

    /**
     * Create product dto.
     *
     * @param productCreationDto the product creation dto
     * @return the product dto
     */
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ProductDto create(@RequestBody @Valid ProductCreationDto productCreationDto) {
        return this.facade.create(productCreationDto);
    }

    /**
     * Add price product dto.
     *
     * @param productId the product id
     * @param priceDto  the price dto
     * @return the product dto
     */
    @PutMapping("/{productId}/add/price")
    @ResponseStatus(HttpStatus.CREATED)
    public final ProductDto addPrice(@PathVariable(PRODUCT_ID) final Integer productId,
                                     @RequestBody @Valid PriceDto priceDto) {
        return this.facade.add(priceDto, productId);
    }

    /**
     * Add category product dto.
     *
     * @param productId  the product id
     * @param categoryId the category id
     * @return the product dto
     */
    @PutMapping("/{productId}/add/category/{categoryId}")
    public final ProductDto addCategory(@PathVariable(PRODUCT_ID) final Integer productId,
                                        @PathVariable(CATEGORY_ID) final Integer categoryId) {
        return this.facade.add(categoryId, productId);
    }

    /**
     * Remove category product dto.
     *
     * @param productId  the product id
     * @param categoryId the category id
     * @return the product dto
     */
    @PutMapping("/{productId}/remove/category/{categoryId}")
    public final ProductDto removeCategory(@PathVariable(PRODUCT_ID) final Integer productId,
                                           @PathVariable(CATEGORY_ID) final Integer categoryId) {
        return this.facade.removeCategory(categoryId, productId);
    }

    /**
     * Remove price product dto.
     *
     * @param productId the product id
     * @param priceId   the price id
     * @return the product dto
     */
    @PutMapping("/{productId}/remove/price/{priceId}")
    public final ProductDto removePrice(@PathVariable(PRODUCT_ID) final Integer productId,
                                        @PathVariable(PRICE_ID) final Integer priceId) {
        return this.facade.removePrice(priceId, productId);
    }

    /**
     * Search all by price list.
     *
     * @param priceDto the price dto
     * @param pageable the pageable
     * @return the list
     */
    @GetMapping("/price")
    public List<ProductDto> searchAllByPrice(@RequestBody @Valid PriceDto priceDto,
                                             Pageable pageable) {
        return this.facade.findAll(pageable, priceDto);
    }

    /**
     * Search all by category list.
     *
     * @param categoryId the category id
     * @param pageable   the pageable
     * @return the list
     */
    @GetMapping("/category/{categoryId}")
    public List<ProductDto> searchAllByCategory(@PathVariable(CATEGORY_ID) final Integer categoryId,
                                                Pageable pageable) {
        return this.facade.findAll(pageable, categoryId);
    }

    /**
     * Search all name list.
     *
     * @param name     the name
     * @param pageable the pageable
     * @return the list
     */
    @GetMapping("/name/{name}")
    public List<ProductDto> searchAllName(@PathVariable("name") final String name,
                                          Pageable pageable) {
        return this.facade.findAll(pageable, name);
    }


}
