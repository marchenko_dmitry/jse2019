package by.epam.training.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import by.epam.training.model.Category;

/**
 * The type Root controller.
 */
@RestController
public class RootController {

    /**
     * Start category.
     *
     * @return the category
     */
    @RequestMapping("/")
    public Category start(){
        return new Category();
    }
}
