package by.epam.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import by.epam.training.dto.BetweenPrice;
import by.epam.training.dto.PriceDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.facade.PriceFacade;
import by.epam.training.model.Price;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * The type Price controller.
 */
@RestController
@RequestMapping("/price")
public class PriceController extends Controller<Price, PriceDto> {

    /**
     * The constant PRODUCT_ID.
     */
    public static final String PRODUCT_ID = "productId";
    private final PriceFacade priceFacade;


    /**
     * Search all between list.
     *
     * @param currency the currency
     * @param from     the from
     * @param to       the to
     * @param pageable the pageable
     * @return the list
     */
    @GetMapping("/between/{currency}/{from}/{to}")
    public final List<PriceDto> searchAllBetween(@PathVariable("currency") final String currency,
                                                 @PathVariable("from") final BigDecimal from,
                                                 @PathVariable("to") final BigDecimal to, final Pageable pageable) {
        return priceFacade.searchAll(pageable, currency, from, to);
    }

    /**
     * Search all between list.
     *
     * @param betweenPrice the between price
     * @param pageable     the pageable
     * @return the list
     */
    @GetMapping("/between")
    public final List<PriceDto> searchAllBetween(@RequestBody @Valid final BetweenPrice betweenPrice,
                                                 final Pageable pageable) {
        return priceFacade.searchAll(pageable, betweenPrice);
    }

    /**
     * Search all by currency list.
     *
     * @param currency the currency
     * @param pageable the pageable
     * @return the list
     */
    @GetMapping("/currency/{currency}")
    public final List<PriceDto> searchAllByCurrency(@PathVariable("currency") String currency, final Pageable pageable) {
        return priceFacade.searchAll(pageable, currency);
    }

    /**
     * Search all by product list.
     *
     * @param productId the product id
     * @param pageable  the pageable
     * @return the list
     */
    @GetMapping("/product/{productId}")
    public final List<PriceDto> searchAllByProduct(@PathVariable(PriceController.PRODUCT_ID) Integer productId,
                                                   final Pageable pageable) {
        return priceFacade.searchAll(pageable, productId);
    }

    /**
     * Search all by product list.
     *
     * @param productDto the product dto
     * @param pageable   the pageable
     * @return the list
     */
    @GetMapping("/product")
    public final List<PriceDto> searchAllByProduct(@RequestBody @Valid final ProductDto productDto,
                                                   final Pageable pageable) {
        return priceFacade.searchAll(pageable, productDto.getId());
    }

    /**
     * Instantiates a new Price controller.
     *
     * @param priceFacade the price facade
     */
    @Autowired
    public PriceController(final PriceFacade priceFacade) {
        super(priceFacade);
        this.priceFacade = priceFacade;
    }

}
