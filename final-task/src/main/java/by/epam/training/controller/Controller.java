package by.epam.training.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import by.epam.training.dto.IDto;
import by.epam.training.facade.Facade;
import by.epam.training.model.IModel;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Abstract controller.
 *
 * @param <M> the type parameter
 * @param <D> the type parameter
 */
public class Controller<M extends IModel, D extends IDto> {

    private final Facade<M, D> facade;

    /**
     * Update d.
     *
     * @param dto the dto
     * @return the d
     */
    @PutMapping("/update")
    public final D update(@RequestBody @Valid D dto) {
        return this.facade.update(dto);
    }

    /**
     * Delete.
     *
     * @param id the id
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/delete/{id}")
    public final void delete(@PathVariable("id") final Integer id) {
        this.facade.remove(id);
    }

    /**
     * Search d.
     *
     * @param id the id
     * @return the d
     */
    @GetMapping("/{id}")
    public final D search(@PathVariable("id") final Integer id) {
        return this.facade.search(id);
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @return the list
     */
    @GetMapping("/list")
    public final List<D> searchAll(Pageable pageable) {
        return this.facade.searchAll(pageable);
    }

    /**
     * Instantiates a new Abstract controller.
     *
     * @param facade the facade
     */
    public Controller(Facade<M, D> facade) {
        this.facade = facade;
    }

}
