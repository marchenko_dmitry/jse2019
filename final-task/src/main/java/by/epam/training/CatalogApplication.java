package by.epam.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;

/**
 * The type Catalog application.
 */
@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
public class CatalogApplication {
    /**
     * Main.
     *
     * @param args the args
     */
    public static void main(final String[] args) {
        SpringApplication.run(CatalogApplication.class, args);
    }
}
