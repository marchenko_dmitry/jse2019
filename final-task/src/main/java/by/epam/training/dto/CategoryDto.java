package by.epam.training.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The type Category dto.
 */
@Data
@Builder
public class CategoryDto implements IDto {
    /**
     * The constant CATEGORY_S_NAME_MUST_BE_NOT_EMPTY.
     */
    public static final String CATEGORY_S_NAME_MUST_BE_NOT_EMPTY = "Category's name must be not empty";
    private Integer id;
    private @NotNull @NotEmpty(message = CATEGORY_S_NAME_MUST_BE_NOT_EMPTY) String name;
    private List<Integer> products;
    private Integer parentId;
    private List<Integer> subCategories;

    /**
     * Instantiates a new Category dto.
     */
    public CategoryDto() {
    }

    /**
     * Instantiates a new Category dto.
     *
     * @param id            the id
     * @param name          the name
     * @param products      the products
     * @param parentId      the parent id
     * @param subCategories the sub categories
     */
    public CategoryDto(final Integer id, final @NotNull @NotEmpty(message = CATEGORY_S_NAME_MUST_BE_NOT_EMPTY) String name,
                       final List<Integer> products, final Integer parentId, final List<Integer> subCategories) {
        this.id = id;
        this.name = name;
        this.products = products;
        this.parentId = parentId;
        this.subCategories = subCategories;
    }
}
