package by.epam.training.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The type Product creation dto.
 */
@Data
@Builder
public class ProductCreationDto implements Serializable {

    /**
     * The constant CURRENCY_MUST_BE_3_CHARACTERS.
     */
    public static final String CURRENCY_MUST_BE_3_CHARACTERS = "Currency must be 3 characters";
    /**
     * The constant AMOUNT_MUST_BE_POSITIVE.
     */
    public static final String AMOUNT_MUST_BE_POSITIVE = "Amount must be positive";
    /**
     * The constant PRODUCT_S_NAME_MUST_BE_NOT_EMPTY.
     */
    public static final String PRODUCT_S_NAME_MUST_BE_NOT_EMPTY = "Product's name must be not empty";
    /**
     * The constant CATEGORY_S_ID_MUST_BE_POSITIVE.
     */
    public static final String CATEGORY_S_ID_MUST_BE_POSITIVE = "Category's ID must be positive";

    private @NotNull @Size(min = 3, max = 3, message = CURRENCY_MUST_BE_3_CHARACTERS) String currency;
    private @NotNull @Positive(message = AMOUNT_MUST_BE_POSITIVE) BigDecimal amount;
    private @NotNull @NotEmpty(message = PRODUCT_S_NAME_MUST_BE_NOT_EMPTY) String name;
    private @NotNull @Positive(message = CATEGORY_S_ID_MUST_BE_POSITIVE) Integer categoryId;

    /**
     * Instantiates a new Product creation dto.
     */
    public ProductCreationDto() {
    }

    /**
     * Instantiates a new Product creation dto.
     *
     * @param currency   the currency
     * @param amount     the amount
     * @param name       the name
     * @param categoryId the category id
     */
    public ProductCreationDto(@NotNull @Size(min = 3, max = 3, message = CURRENCY_MUST_BE_3_CHARACTERS) String currency,
                              @NotNull @Positive(message = AMOUNT_MUST_BE_POSITIVE) BigDecimal amount,
                              @NotNull @NotEmpty(message = PRODUCT_S_NAME_MUST_BE_NOT_EMPTY) String name,
                              @NotNull @Positive(message = CATEGORY_S_ID_MUST_BE_POSITIVE) Integer categoryId) {
        this.currency = currency;
        this.amount = amount;
        this.name = name;
        this.categoryId = categoryId;
    }
}
