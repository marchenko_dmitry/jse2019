package by.epam.training.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The type Between price.
 */
@Data
@Builder
public class BetweenPrice implements Serializable {

    /**
     * The constant AMOUNT_MUST_BE_POSITIVE.
     */
    public static final String AMOUNT_MUST_BE_POSITIVE = "Amount must be positive";
    /**
     * The constant CURRENCY_MUST_BE_3_CHARACTERS.
     */
    public static final String CURRENCY_MUST_BE_3_CHARACTERS = "Currency must be 3 characters";
    private @NotNull @Positive(message = AMOUNT_MUST_BE_POSITIVE) BigDecimal from;

    private @NotNull @Positive(message = AMOUNT_MUST_BE_POSITIVE) BigDecimal to;

    private @NotNull @Size(min = 3, max = 3, message = CURRENCY_MUST_BE_3_CHARACTERS) String currency;

    /**
     * Instantiates a new Between price.
     */
    public BetweenPrice() {
    }

    /**
     * Instantiates a new Between price.
     *
     * @param from     the from
     * @param to       the to
     * @param currency the currency
     */
    public BetweenPrice(@NotNull @Positive(message = AMOUNT_MUST_BE_POSITIVE) BigDecimal from,
                        @NotNull @Positive(message = AMOUNT_MUST_BE_POSITIVE) BigDecimal to,
                        @NotNull @Size(min = 3, max = 3, message = CURRENCY_MUST_BE_3_CHARACTERS) String currency) {
        this.from = from;
        this.to = to;
        this.currency = currency;
    }
}
