package by.epam.training.dto;

import java.io.Serializable;

/**
 * The interface Dto.
 */
public interface IDto extends Serializable {

    /**
     * Gets id.
     *
     * @return the id
     */
    Integer getId();
}
