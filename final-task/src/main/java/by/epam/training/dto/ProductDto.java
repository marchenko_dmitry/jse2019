package by.epam.training.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The type Product dto.
 */
@Data
@Builder
public class ProductDto implements IDto {
    /**
     * The constant PRODUCT_S_NAME_MUST_BE_NOT_EMPTY.
     */
    public static final String PRODUCT_S_NAME_MUST_BE_NOT_EMPTY = "Product's name must be not empty";
    private Integer id;
    private @NotNull @NotEmpty(message = ProductDto.PRODUCT_S_NAME_MUST_BE_NOT_EMPTY) String name;
    private List<Integer> prices;
    private List<Integer> categories;

    /**
     * Instantiates a new Product dto.
     */
    public ProductDto() {
    }

    /**
     * Instantiates a new Product dto.
     *
     * @param id         the id
     * @param name       the name
     * @param prices     the prices
     * @param categories the categories
     */
    public ProductDto(final Integer id, final @NotNull @NotEmpty(message = PRODUCT_S_NAME_MUST_BE_NOT_EMPTY) String name,
                      final List<Integer> prices, final List<Integer> categories) {
        this.id = id;
        this.name = name;
        this.prices = prices;
        this.categories = categories;
    }
}
