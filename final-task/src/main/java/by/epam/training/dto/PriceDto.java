package by.epam.training.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * The type Price dto.
 */
@Data
@Builder
public class PriceDto implements IDto {
    /**
     * The constant AMOUNT_MUST_BE_POSITIVE.
     */
    public static final String AMOUNT_MUST_BE_POSITIVE = "Amount must be positive";
    /**
     * The constant CURRENCY_MUST_BE_3_CHARACTERS.
     */
    public static final String CURRENCY_MUST_BE_3_CHARACTERS = "Currency must be 3 characters";
    private Integer id;
    private @Size(min = 3, max = 3, message = CURRENCY_MUST_BE_3_CHARACTERS) @NotNull String currency;
    private @Positive(message = AMOUNT_MUST_BE_POSITIVE) @NotNull BigDecimal amount;
    private Integer productId;

    /**
     * Instantiates a new Price dto.
     */
    public PriceDto() {
    }

    /**
     * Instantiates a new Price dto.
     *
     * @param id        the id
     * @param currency  the currency
     * @param amount    the amount
     * @param productId the product id
     */
    public PriceDto(final Integer id, final @Size(min = 3, max = 3, message = CURRENCY_MUST_BE_3_CHARACTERS) @NotNull String currency,
                    final @Positive(message = AMOUNT_MUST_BE_POSITIVE) @NotNull BigDecimal amount, final Integer productId) {
        this.id = id;
        this.currency = currency;
        this.amount = amount;
        this.productId = productId;
    }
}
