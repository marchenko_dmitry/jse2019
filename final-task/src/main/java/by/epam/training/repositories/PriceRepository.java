package by.epam.training.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import by.epam.training.model.Price;
import by.epam.training.model.Product;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

/**
 * The interface Price repository.
 */
public interface PriceRepository extends JpaRepository<Price, Integer> {

    /**
     * Find all by amount between and currency list.
     *
     * @param from     the from
     * @param to       the to
     * @param currency the currency
     * @param pageable the pageable
     * @return the list
     */
    List<Price> findAllByAmountBetweenAndCurrency(BigDecimal from, BigDecimal to, Currency currency, Pageable pageable);

    /**
     * Find all by currency list.
     *
     * @param currency the currency
     * @param pageable the pageable
     * @return the list
     */
    List<Price> findAllByCurrency(Currency currency, Pageable pageable);

    /**
     * Find all by product list.
     *
     * @param product  the product
     * @param pageable the pageable
     * @return the list
     */
    List<Price> findAllByProduct(Product product, Pageable pageable);

    /**
     * Count all by product integer.
     *
     * @param product the product
     * @return the integer
     */
    Integer countAllByProduct(Product product);

}
