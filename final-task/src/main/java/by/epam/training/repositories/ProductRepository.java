package by.epam.training.repositories;

import by.epam.training.model.Category;
import by.epam.training.model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

/**
 * The interface Product repository.
 */
public interface ProductRepository extends JpaRepository<Product, Integer> {

    /**
     * The constant SELECT_PROD_FROM_PRICE.
     */
    String SELECT_PROD_FROM_PRICE = "SELECT prod FROM Price price INNER JOIN Product prod ON prod.id = price.product.id  " +
            "WHERE price.amount = :amount AND price.currency LIKE :currency";
    /**
     * The constant AMOUNT.
     */
    String AMOUNT = "amount";
    /**
     * The constant CURRENCY.
     */
    String CURRENCY = "currency";

    /**
     * Find all by name list.
     *
     * @param name     the name
     * @param pageable the pageable
     * @return the list
     */
    List<Product> findAllByName(String name, Pageable pageable);

    /**
     * Find all by price list.
     *
     * @param amount   the amount
     * @param currency the currency
     * @param pageable the pageable
     * @return the list
     */
    @Query(value = SELECT_PROD_FROM_PRICE, nativeQuery = true)
    List<Product> findAllByPrice(@Param(AMOUNT) BigDecimal amount, @Param(CURRENCY) Currency currency,
                                 Pageable pageable);

    /**
     * Find all by categories list.
     *
     * @param category the category
     * @param pageable the pageable
     * @return the list
     */
    List<Product> findAllByCategories(Category category, Pageable pageable);

}
