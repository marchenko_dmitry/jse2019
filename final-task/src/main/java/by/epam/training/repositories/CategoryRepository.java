package by.epam.training.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import by.epam.training.model.Category;

import java.util.List;

/**
 * The interface Category repository.
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    /**
     * Find all by name list.
     *
     * @param name     the name
     * @param pageable the pageable
     * @return the list
     */
    List<Category> findAllByName(String name, Pageable pageable);
}
