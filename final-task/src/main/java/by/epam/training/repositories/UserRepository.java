package by.epam.training.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import by.epam.training.model.User;

import java.util.Optional;

/**
 * The interface User repository.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * Find by name optional.
     *
     * @param name the name
     * @return the optional
     */
    Optional<User> findByName(String name);
}
