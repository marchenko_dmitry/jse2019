package by.epam.training.services;

import by.epam.training.exception.BadRequestException;
import by.epam.training.exception.NotFoundException;
import by.epam.training.model.IModel;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * The type Abstract service.
 *
 * @param <M> the type parameter
 */
public abstract class AbstractService<M extends IModel> implements IService<M> {
    private Class<M> typeClass;

    private final JpaRepository<M, Integer> repository;

    @Override
    public M search(final Integer id) {
        final Optional<M> byId = repository.findById(id);
        return byId
                .orElseThrow(() -> NotFoundException.createNotFoundException(typeClass.getSimpleName(), id));
    }

    @Override
    public M get(final Integer id) {
        final Optional<M> byId = repository.findById(id);
        return byId
                .orElseThrow(() -> BadRequestException.createBadRequestException(typeClass.getSimpleName(), id));
    }

    @Override
    public List<M> searchAll(final Pageable pageable) {
        return repository.findAll(pageable).getContent();
    }

    @Override
    public void delete(final M m) {
        repository.delete(m);
    }

    @Override
    public M save(final M m) {
        return repository.save(m);
    }

    /**
     * Instantiates a new Abstract service.
     *
     * @param repository the repository
     */
    protected AbstractService(final JpaRepository<M, Integer> repository) {
        this.repository = repository;
    }

    /**
     * Gets type class.
     *
     * @return the type class
     */
    public Class<M> getTypeClass() {
        return this.typeClass;
    }

    /**
     * Sets type class.
     *
     * @param typeClass the type class
     */
    public void setTypeClass(final Class<M> typeClass) {
        this.typeClass = typeClass;
    }

    /**
     * Gets repository.
     *
     * @return the repository
     */
    public JpaRepository<M, Integer> getRepository() {
        return this.repository;
    }
}
