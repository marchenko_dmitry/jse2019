package by.epam.training.services;

import org.springframework.data.domain.Pageable;
import by.epam.training.model.IModel;

import java.util.List;

/**
 * The interface Service.
 *
 * @param <M> the type parameter
 */
public interface IService<M extends IModel> {

    /**
     * Search m.
     *
     * @param id the id
     * @return the m
     */
    M search(Integer id);

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @return the list
     */
    List<M> searchAll(Pageable pageable);

    /**
     * Delete.
     *
     * @param m the m
     */
    void delete(M m);

    /**
     * Save m.
     *
     * @param m the m
     * @return the m
     */
    M save(M m);

    /**
     * Get m.
     *
     * @param id the id
     * @return the m
     */
    M get(Integer id);
}
