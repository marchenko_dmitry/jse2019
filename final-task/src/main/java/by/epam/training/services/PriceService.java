package by.epam.training.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import by.epam.training.model.Price;
import by.epam.training.model.Product;
import by.epam.training.repositories.PriceRepository;
import by.epam.training.services.AbstractService;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

/**
 * The type Price service.
 */
@Service
@Transactional
public class PriceService extends AbstractService<Price> {

    private final PriceRepository repository;

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param currency the currency
     * @return the list
     */
    public List<Price> searchAll(final Pageable pageable, final String currency) {
        return repository.findAllByCurrency(Currency.getInstance(currency.toUpperCase()), pageable);
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param product  the product
     * @return the list
     */
    public List<Price> searchAll(final Pageable pageable, final Product product) {
        return repository.findAllByProduct(product, pageable);
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param currency the currency
     * @param from     the from
     * @param to       the to
     * @return the list
     */
    public List<Price> searchAll(final Pageable pageable,
                               final String currency, final BigDecimal from,
                               final BigDecimal to) {
        return repository.findAllByAmountBetweenAndCurrency(from, to,
                Currency.getInstance(currency.toUpperCase()), pageable);
    }

    /**
     * Count all by product int.
     *
     * @param product the product
     * @return the int
     */
    public int countAllByProduct(final Product product) {
        return repository.countAllByProduct(product);
    }


    /**
     * Instantiates a new Price service.
     *
     * @param repository the repository
     */
    @Autowired
    public PriceService(final PriceRepository repository) {
        super(repository);
        this.repository = repository;
        setTypeClass(Price.class);
    }
}
