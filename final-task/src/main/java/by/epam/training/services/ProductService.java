package by.epam.training.services;

import by.epam.training.model.Category;
import by.epam.training.model.Product;
import by.epam.training.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

/**
 * The type Product service.
 */
@Service
@Transactional
public class ProductService extends AbstractService<Product> {

    private final ProductRepository repository;

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param name     the name
     * @return the list
     */
    public List<Product> searchAll(final Pageable pageable, final String name) {
        return repository.findAllByName(name, pageable);
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param category the category
     * @return the list
     */
    public List<Product> searchAll(final Pageable pageable, final Category category) {
        return repository.findAllByCategories(category, pageable);
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param amount   the amount
     * @param currency the currency
     * @return the list
     */
    public List<Product> searchAll(final Pageable pageable, final BigDecimal amount, final String currency) {
        return repository.findAllByPrice(amount, Currency.getInstance(currency.toUpperCase()), pageable);
    }

    /**
     * Instantiates a new Product service.
     *
     * @param repository the repository
     */
    @Autowired
    public ProductService(final ProductRepository repository) {
        super(repository);
        this.repository = repository;
        setTypeClass(Product.class);
    }
}
