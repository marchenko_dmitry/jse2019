package by.epam.training.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import by.epam.training.model.Category;
import by.epam.training.repositories.CategoryRepository;
import by.epam.training.services.AbstractService;

import java.util.List;

/**
 * The type Category service.
 */
@Service
@Transactional
public class CategoryService extends AbstractService<Category> {

    private final CategoryRepository repository;

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param name     the name
     * @return the list
     */
    public List<Category> searchAll(final Pageable pageable, final String name) {
        return repository.findAllByName(name, pageable);
    }

    /**
     * Instantiates a new Category service.
     *
     * @param repository the repository
     */
    @Autowired
    public CategoryService(final CategoryRepository repository) {
        super(repository);
        this.repository = repository;
        setTypeClass(Category.class);
    }
}
