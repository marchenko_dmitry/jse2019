package by.epam.training.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import by.epam.training.dto.PriceDto;

/**
 * The type Wrong product in price exception.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public final class WrongProductInPriceException extends AbstractException {

    private WrongProductInPriceException(final PriceDto priceDto) {
        super("Wrong Product in Price " + priceDto);
    }

    /**
     * Create wrong product in price exception wrong product in price exception.
     *
     * @param priceDto the price dto
     * @return the wrong product in price exception
     */
    public static WrongProductInPriceException createWrongProductInPriceException(final PriceDto priceDto) {
        return new WrongProductInPriceException(priceDto);
    }
}
