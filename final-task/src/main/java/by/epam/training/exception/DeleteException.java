package by.epam.training.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Delete exception.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DeleteException extends AbstractException {

    private DeleteException(final String message) {
        super("Can't delete " + message);
    }

    /**
     * Create delete exception delete exception.
     *
     * @param message the message
     * @return the delete exception
     */
    public static DeleteException createDeleteException(final String message) {
        return new DeleteException(message);
    }
}
