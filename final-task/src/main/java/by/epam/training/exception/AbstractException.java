package by.epam.training.exception;

/**
 * The type Abstract exception.
 */
public abstract class AbstractException extends IllegalArgumentException {
    private final String message;

    /**
     * Instantiates a new Abstract exception.
     *
     * @param message the message
     */
    public AbstractException(final String message) {
        this.message = message;
    }

    public final String getMessage() {
        return message;
    }
}
