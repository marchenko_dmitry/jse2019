package by.epam.training.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import by.epam.training.dto.IDto;

/**
 * The type Bad request exception.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public final class BadRequestException extends AbstractException {

    private BadRequestException(final String model, final int id) {
        super("Can't find " + model + " with ID = " + id);
    }

    private BadRequestException(final String model, final IDto dto) {
        super("Can't find " + model + " = " + dto);
    }

    /**
     * Create bad request exception bad request exception.
     *
     * @param model the model
     * @param id    the id
     * @return the bad request exception
     */
    public static BadRequestException createBadRequestException(final String model, final int id) {
        return new BadRequestException(model, id);
    }

    /**
     * Create bad request exception bad request exception.
     *
     * @param model the model
     * @param dto   the dto
     * @return the bad request exception
     */
    public static BadRequestException createBadRequestException(final String model, final IDto dto) {
        return new BadRequestException(model, dto);
    }
}
