package by.epam.training.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Not found exception.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public final class NotFoundException extends AbstractException {

    /**
     * Instantiates a new Not found exception.
     *
     * @param model the model
     * @param id    the id
     */
    public NotFoundException(final String model, final int id) {
        super("Can't find " + model + " with ID = " + id);
    }


    /**
     * Create not found exception not found exception.
     *
     * @param model the model
     * @param id    the id
     * @return the not found exception
     */
    public static NotFoundException createNotFoundException(final String model, final int id) {
        return new NotFoundException(model, id);
    }
}
