package by.epam.training.security;

import by.epam.training.model.User;
import by.epam.training.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The type Simple db user details service.
 */
@Service
public class SimpleDbUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    /**
     * Instantiates a new Simple db user details service.
     *
     * @param userRepository the user repository
     */
    @Autowired
    public SimpleDbUserDetailsService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(final String name) {
        final Optional<User> byName = userRepository.findByName(name);
        final User user = byName.orElseThrow(() -> new UsernameNotFoundException(name));
        return new SimpleDbUserPrinciple(user);
    }
}

