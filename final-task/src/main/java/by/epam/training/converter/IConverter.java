package by.epam.training.converter;

import by.epam.training.dto.IDto;
import by.epam.training.model.IModel;

/**
 * The interface Converter.
 *
 * @param <M> the type parameter
 * @param <D> the type parameter
 */
public interface IConverter<M extends IModel, D extends IDto> {

    /**
     * Convert d.
     *
     * @param model the model
     * @return the d
     */
    D convert(M model);

    /**
     * Convert m.
     *
     * @param dto   the dto
     * @param model the model
     * @return the m
     */
    M convert(D dto, M model);

    /**
     * Is null.
     *
     * @param obj       the obj
     * @param className the class name
     */
    default void isNull(final Object obj, final String className) {
        if (obj == null) {
            throw new IllegalStateException(className + " is null");
        }
    }
}
