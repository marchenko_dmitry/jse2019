package by.epam.training.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import by.epam.training.dto.CategoryDto;
import by.epam.training.model.Category;
import by.epam.training.model.Product;
import by.epam.training.services.CategoryService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Category converter.
 */
@Service
public class CategoryConverter implements IConverter<Category, CategoryDto> {

    private final CategoryService categoryService;

    /**
     * Instantiates a new Category converter.
     *
     * @param categoryService the category service
     */
    @Autowired
    public CategoryConverter(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public CategoryDto convert(final Category category) {
        final String simpleName = Category.class.getSimpleName();
        isNull(category, simpleName);

        CategoryDto dto = new CategoryDto();

        dto.setId(category.getId());
        dto.setName(category.getName());

        if (category.getParent() != null) {
            dto.setParentId(category.getParent().getId());
        }

        final List<Integer> subCategories = category.getSubCategories().stream()
                .map(Category::getId)
                .collect(Collectors.toList());
        dto.setSubCategories(subCategories);

        final List<Integer> productList = category.getProducts().stream()
                .map(Product::getId)
                .collect(Collectors.toList());
        dto.setProducts(productList);

        return dto;
    }

    /**
     * Convert category.
     *
     * @param categoryDto the category dto
     * @return the category
     */
    public Category convert(final CategoryDto categoryDto) {
        isNull(categoryDto, CategoryDto.class.getSimpleName());

        final Category parentCategory = getParentCategory(categoryDto.getParentId());

        Category category = new Category();

        category.setName(categoryDto.getName());
        category.setParent(parentCategory);

        return category;
    }

    private Category getParentCategory(final Integer parentId) {
        if (parentId == null) {
            return null;
        }

        return categoryService.search(parentId);
    }

    @Override
    public Category convert(final CategoryDto categoryDto, final Category category) {

        final String simpleName = Category.class.getSimpleName();
        isNull(category, simpleName);
        final String simpleName1 = CategoryDto.class.getSimpleName();
        isNull(categoryDto, simpleName1);

        final Integer parentId = categoryDto.getParentId();
        final Category parentCategory = getParentCategory(parentId);

        category.setName(categoryDto.getName());
        category.setParent(parentCategory);

        return category;
    }
}
