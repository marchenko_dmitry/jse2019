package by.epam.training.converter;

import org.springframework.stereotype.Service;
import by.epam.training.dto.PriceDto;
import by.epam.training.exception.WrongProductInPriceException;
import by.epam.training.converter.IConverter;
import by.epam.training.model.Price;

import java.util.Currency;

/**
 * The type Price converter.
 */
@Service
public class PriceConverter implements IConverter<Price, PriceDto> {

    @Override
    public final PriceDto convert(final Price price) {
        isNull(price, Price.class.getSimpleName());

        PriceDto dto = new PriceDto();

        dto.setId(price.getId());
        dto.setAmount(price.getAmount());
        dto.setCurrency(price.getCurrency().getCurrencyCode());
        if (price.getProduct() != null) {
            dto.setProductId(price.getProduct().getId());
        }
        return dto;
    }

    /**
     * Convert price.
     *
     * @param priceDto the price dto
     * @return the price
     */
    public final Price convert(final PriceDto priceDto) {
        isNull(priceDto, PriceDto.class.getSimpleName());

        Price price = new Price();
        price.setAmount(priceDto.getAmount());
        price.setCurrency(Currency.getInstance(priceDto.getCurrency().toUpperCase()));
        return price;
    }

    @Override
    public final Price convert(final PriceDto priceDto, final Price price) {
        isNull(priceDto, PriceDto.class.getSimpleName());
        isNull(price, Price.class.getSimpleName());

        if (priceDto.getProductId() != null && !price.getProduct().getId().equals(priceDto.getProductId())) {
            throw WrongProductInPriceException.createWrongProductInPriceException(priceDto);
        }

        price.setAmount(priceDto.getAmount());
        price.setCurrency(Currency.getInstance(priceDto.getCurrency().toUpperCase()));

        return price;
    }
}
