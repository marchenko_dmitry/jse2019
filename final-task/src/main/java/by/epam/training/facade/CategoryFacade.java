package by.epam.training.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import by.epam.training.dto.CategoryDto;
import by.epam.training.exception.DeleteException;
import by.epam.training.converter.CategoryConverter;
import by.epam.training.model.Category;
import by.epam.training.services.CategoryService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Category facade.
 */
@Service
@Transactional
public class CategoryFacade extends AbstractFacade<Category, CategoryDto> {

    /**
     * The constant BECAUSE_IT_HAS_SUB_CATEGORIES.
     */
    public static final String BECAUSE_IT_HAS_SUB_CATEGORIES = ", because it has sub categories";
    /**
     * The constant BECAUSE_IT_HAS_PRODUCTS.
     */
    public static final String BECAUSE_IT_HAS_PRODUCTS = ", because it has products";
    private final CategoryService categoryService;
    private final CategoryConverter categoryConverter;

    /**
     * Create category dto.
     *
     * @param categoryDto the category dto
     * @return the category dto
     */
    public CategoryDto create(final CategoryDto categoryDto) {
        final Category converted = categoryConverter.convert(categoryDto);
        final Category saved = categoryService.save(converted);
        return categoryConverter.convert(saved);
    }

    @Override
    protected void checkForDelete(final Category category) {
        if (!category.getSubCategories().isEmpty()) {
            throw DeleteException.createDeleteException(category + BECAUSE_IT_HAS_SUB_CATEGORIES);
        }
        if (!category.getProducts().isEmpty()) {
            throw DeleteException.createDeleteException(category + BECAUSE_IT_HAS_PRODUCTS);
        }
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param name     the name
     * @return the list
     */
    public List<CategoryDto> searchAll(final Pageable pageable, final String name) {
        return categoryService.searchAll(pageable, name).stream()
                .map(categoryConverter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Instantiates a new Category facade.
     *
     * @param categoryConverter the category converter
     * @param categoryService   the category service
     */
    @Autowired
    public CategoryFacade(final CategoryConverter categoryConverter,
                          final CategoryService categoryService) {
        super(categoryConverter, categoryService);
        this.categoryService = categoryService;
        this.categoryConverter = categoryConverter;
    }

}
