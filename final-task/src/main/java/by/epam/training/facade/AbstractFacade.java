package by.epam.training.facade;

import org.springframework.data.domain.Pageable;
import by.epam.training.dto.IDto;
import by.epam.training.exception.BadRequestException;
import by.epam.training.converter.IConverter;
import by.epam.training.model.IModel;
import by.epam.training.services.IService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Abstract facade.
 *
 * @param <M> the type parameter
 * @param <D> the type parameter
 */
public abstract class AbstractFacade<M extends IModel, D extends IDto> implements Facade<M, D> {

    private final IConverter<M, D> converter;
    private final IService<M> service;

    public M get(final Integer id) {
        return service.search(id);
    }

    private M get(final D d) {
        if (d.getId() == null) {
            throw BadRequestException.createBadRequestException(d.getClass().getSimpleName(), d);
        }
        return get(d.getId());
    }

    @Override
    public D update(final D dto) {
        M m = converter.convert(dto, get(dto));
        final M saved = service.save(m);
        return converter.convert(saved);
    }

    @Override
    public void remove(final Integer id) {
        final M m = get(id);
        checkForDelete(m);
        service.delete(m);
    }

    @Override
    public D search(final Integer id) {
        return converter.convert(service.search(id));
    }

    @Override
    public List<D> searchAll(final Pageable pageable) {
        return service.searchAll(pageable)
                .stream()
                .map(converter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Check for delete.
     *
     * @param m the m
     */
    protected abstract void checkForDelete(M m);


    /**
     * Instantiates a new Abstract facade.
     *
     * @param converter the converter
     * @param service   the service
     */
    public AbstractFacade(final IConverter<M, D> converter,
                          final IService<M> service) {
        this.converter = converter;
        this.service = service;
    }

}
