package by.epam.training.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import by.epam.training.dto.BetweenPrice;
import by.epam.training.dto.PriceDto;
import by.epam.training.exception.DeleteException;
import by.epam.training.converter.PriceConverter;
import by.epam.training.model.Price;
import by.epam.training.services.PriceService;
import by.epam.training.services.ProductService;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Price facade.
 */
@Service
@Transactional
public class PriceFacade extends AbstractFacade<Price, PriceDto> {

    /**
     * The constant LAST.
     */
    public static final String LAST = "last ";
    /**
     * The constant IN.
     */
    public static final String IN = " in ";
    private final PriceService priceService;
    private final PriceConverter priceConverter;
    private final ProductService productService;

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param currency the currency
     * @param from     the from
     * @param to       the to
     * @return the list
     */
    public List<PriceDto> searchAll(final Pageable pageable,
                                  final String currency, final BigDecimal from,
                                  final BigDecimal to) {
        return priceService.searchAll(pageable, currency, from, to).stream()
                .map(priceConverter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Search all list.
     *
     * @param pageable     the pageable
     * @param betweenPrice the between price
     * @return the list
     */
    public List<PriceDto> searchAll(final Pageable pageable, final BetweenPrice betweenPrice) {
        return searchAll(pageable, betweenPrice.getCurrency(), betweenPrice.getFrom(), betweenPrice.getTo());
    }

    /**
     * Search all list.
     *
     * @param pageable  the pageable
     * @param productId the product id
     * @return the list
     */
    public List<PriceDto> searchAll(final Pageable pageable, final Integer productId) {
        return priceService.searchAll(pageable, productService.get(productId)).stream()
                .map(priceConverter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @param currency the currency
     * @return the list
     */
    public List<PriceDto> searchAll(final Pageable pageable, final String currency) {
        return priceService.searchAll(pageable, currency).stream()
                .map(priceConverter::convert)
                .collect(Collectors.toList());
    }

    @Override
    protected void checkForDelete(final Price price) {
        if (priceService.countAllByProduct(price.getProduct()) < 2) {
            throw DeleteException.createDeleteException(LAST + price + IN + price.getProduct());
        }
    }

    /**
     * Instantiates a new Price facade.
     *
     * @param priceService   the price service
     * @param priceConverter the price converter
     * @param productService the product service
     */
    @Autowired
    public PriceFacade(final PriceService priceService,
                       final PriceConverter priceConverter,
                       final ProductService productService) {
        super(priceConverter, priceService);
        this.priceService = priceService;
        this.priceConverter = priceConverter;
        this.productService = productService;
    }
}
