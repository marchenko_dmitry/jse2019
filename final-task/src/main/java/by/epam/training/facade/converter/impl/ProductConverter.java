package by.epam.training.facade.converter.impl;

import org.springframework.stereotype.Service;
import by.epam.training.dto.ProductCreationDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.facade.converter.IConverter;
import by.epam.training.model.Category;
import by.epam.training.model.Price;
import by.epam.training.model.Product;

import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Product converter.
 */
@Service
public class ProductConverter implements IConverter<Product, ProductDto> {

    @Override
    public ProductDto convert(final Product product) {
        isNull(product, Product.class.getSimpleName());

        ProductDto dto = new ProductDto();

        dto.setId(product.getId());
        dto.setName(product.getName());

        final List<Integer> categoryList = product.getCategories().stream()
                .map(Category::getId)
                .collect(Collectors.toList());
        dto.setCategories(categoryList);

        final List<Integer> priceList = product.getPrices().stream()
                .map(Price::getId)
                .collect(Collectors.toList());
        dto.setPrices(priceList);

        return dto;
    }

    /**
     * Convert product.
     *
     * @param productCreationDto the product creation dto
     * @param category           the category
     * @return the product
     */
    public Product convert(final ProductCreationDto productCreationDto, final Category category) {
        isNull(productCreationDto, ProductCreationDto.class.getSimpleName());
        isNull(category, Category.class.getSimpleName());

        final Price price = new Price(productCreationDto.getAmount(),
                Currency.getInstance(productCreationDto.getCurrency().toUpperCase()));

        final Product product = new Product();
        product.setName(productCreationDto.getName());
        product.getPrices().add(price);
        price.setProduct(product);
        product.getCategories().add(category);

        return product;
    }

    @Override
    public Product convert(final ProductDto productDto, final Product product) {
        isNull(product, Product.class.getSimpleName());
        isNull(productDto, ProductDto.class.getSimpleName());

        product.setName(productDto.getName());
        return product;
    }
}
