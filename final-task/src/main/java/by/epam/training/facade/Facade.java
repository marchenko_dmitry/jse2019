package by.epam.training.facade;

import org.springframework.data.domain.Pageable;
import by.epam.training.dto.IDto;
import by.epam.training.model.IModel;

import java.util.List;

/**
 * The interface Facade.
 *
 * @param <M> the type parameter
 * @param <D> the type parameter
 */
public interface Facade<M extends IModel, D extends IDto> {

    /**
     * Search d.
     *
     * @param id the id
     * @return the d
     */
    D search(Integer id);

    /**
     * Search all list.
     *
     * @param pageable the pageable
     * @return the list
     */
    List<D> searchAll(Pageable pageable);

    /**
     * Update d.
     *
     * @param dto the dto
     * @return the d
     */
    D update(D dto);

    /**
     * Remove.
     *
     * @param id the id
     */
    void remove(Integer id);

    /**
     * Get m.
     *
     * @param id the id
     * @return the m
     */
    M get(Integer id);
}
