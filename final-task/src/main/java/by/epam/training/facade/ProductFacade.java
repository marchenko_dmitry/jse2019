package by.epam.training.facade;

import by.epam.training.dto.PriceDto;
import by.epam.training.dto.ProductCreationDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.exception.DeleteException;
import by.epam.training.converter.PriceConverter;
import by.epam.training.converter.ProductConverter;
import by.epam.training.model.Category;
import by.epam.training.model.Price;
import by.epam.training.model.Product;
import by.epam.training.services.PriceService;
import by.epam.training.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Product facade.
 */
@Service
@Transactional
public class ProductFacade extends AbstractFacade<Product, ProductDto> {

    /**
     * The constant LAST_CATEGORY_IN_PRODUCT.
     */
    public static final String LAST_CATEGORY_IN_PRODUCT = "last Category in Product";
    /**
     * The constant LAST_PRICE_IN_PRODUCT.
     */
    public static final String LAST_PRICE_IN_PRODUCT = "last Price in Product";
    private final CategoryFacade categoryFacade;
    private final ProductConverter productConverter;
    private final ProductService productService;
    private final PriceConverter priceConverter;
    private final PriceService priceService;

    /**
     * Instantiates a new Product facade.
     *
     * @param productService   the product service
     * @param categoryFacade   the category facade
     * @param productConverter the product converter
     * @param priceConverter   the price converter
     * @param priceService     the price service
     */
    @Autowired
    public ProductFacade(final ProductService productService,
                         final CategoryFacade categoryFacade,
                         final ProductConverter productConverter,
                         final PriceConverter priceConverter,
                         final PriceService priceService) {
        super(productConverter, productService);
        this.productService = productService;
        this.categoryFacade = categoryFacade;
        this.productConverter = productConverter;
        this.priceConverter = priceConverter;
        this.priceService = priceService;
    }

    /**
     * Find all list.
     *
     * @param pageable   the pageable
     * @param categoryId the category id
     * @return the list
     */
    public List<ProductDto> findAll(final Pageable pageable, final Integer categoryId) {
        return productService.searchAll(pageable, categoryFacade.get(categoryId)).stream()
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Find all list.
     *
     * @param pageable the pageable
     * @param name     the name
     * @return the list
     */
    public List<ProductDto> findAll(final Pageable pageable, final String name) {
        return productService.searchAll(pageable, name).stream()
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Find all list.
     *
     * @param pageable the pageable
     * @param priceDto the price dto
     * @return the list
     */
    public List<ProductDto> findAll(final Pageable pageable, final PriceDto priceDto) {
        return productService.searchAll(pageable, priceDto.getAmount(), priceDto.getCurrency()).stream()
                .map(productConverter::convert)
                .collect(Collectors.toList());
    }

    /**
     * Create product dto.
     *
     * @param productCreationDto the product creation dto
     * @return the product dto
     */
    public ProductDto create(final ProductCreationDto productCreationDto) {
        final Category category = categoryFacade.get(productCreationDto.getCategoryId());
        final Product converted = productConverter.convert(productCreationDto, category);

        final Product saved = productService.save(converted);
        return productConverter.convert(saved);
    }

    /**
     * Add product dto.
     *
     * @param priceDto  the price dto
     * @param productId the product id
     * @return the product dto
     */
    public ProductDto add(final PriceDto priceDto, final Integer productId) {
        final Product found = get(productId);
        final Price price = priceConverter.convert(priceDto);

        found.getPrices().add(price);
        price.setProduct(found);
        final Product saved = productService.save(found);
        return productConverter.convert(saved);
    }

    /**
     * Add product dto.
     *
     * @param categoryId the category id
     * @param productId  the product id
     * @return the product dto
     */
    public ProductDto add(final Integer categoryId, final Integer productId) {
        final Category category = categoryFacade.get(categoryId);
        final Product found = get(productId);

        found.getCategories().add(category);

        final Product saved = productService.save(found);
        return productConverter.convert(saved);
    }

    /**
     * Remove category product dto.
     *
     * @param categoryId the category id
     * @param productId  the product id
     * @return the product dto
     */
    public ProductDto removeCategory(final Integer categoryId, final Integer productId) {
        final Product found = get(productId);

        if (found.getCategories().size() < 2) {
            throw DeleteException.createDeleteException(LAST_CATEGORY_IN_PRODUCT);
        }

        final Category category = categoryFacade.get(categoryId);
        found.getCategories().remove(category);

        final Product saved = productService.save(found);
        return productConverter.convert(saved);
    }

    /**
     * Remove price product dto.
     *
     * @param priceId   the price id
     * @param productId the product id
     * @return the product dto
     */
    public ProductDto removePrice(final Integer priceId, final Integer productId) {
        final Product found = get(productId);

        if (found.getPrices().size() < 2) {
            throw DeleteException.createDeleteException(LAST_PRICE_IN_PRODUCT);
        }

        final Price price = priceService.get(priceId);
        found.getPrices().remove(price);

        final Product saved = productService.save(found);
        return productConverter.convert(saved);
    }


    @Override
    protected void checkForDelete(final Product product) {

    }
}
