package by.epam.training.model;

/**
 * The interface Model.
 */
public interface IModel {

    /**
     * Gets id.
     *
     * @return the id
     */
    Integer getId();

}
