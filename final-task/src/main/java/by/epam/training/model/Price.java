package by.epam.training.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Currency;

/**
 * The type Price.
 */
@Entity
@Data
public class Price implements IModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private Currency currency;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    /**
     * Instantiates a new Price.
     */
    public Price() {
    }

    /**
     * Instantiates a new Price.
     *
     * @param amount   the amount
     * @param currency the currency
     */
    public Price(final BigDecimal amount, final Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    @Override
    public String toString() {
        return product + " ";
    }
}
