package by.epam.training.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Category.
 */
@Entity
@Data
public class Category implements IModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @ManyToMany
    @JoinTable(
            name = "category_product",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> products = new ArrayList<>();

    @OneToMany(mappedBy = "parent")
    private List<Category> subCategories = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Category parent;

    /**
     * Instantiates a new Category.
     */
    public Category() {
    }

    /**
     * Instantiates a new Category.
     *
     * @param name the name
     */
    public Category(final String name) {
        this.name = name;
    }

    /**
     * Instantiates a new Category.
     *
     * @param name   the name
     * @param parent the parent
     */
    public Category(final String name, final Category parent) {
        this.name = name;
        this.parent = parent;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets products.
     *
     * @return the products
     */
    public List<Product> getProducts() {
        return this.products;
    }

    /**
     * Sets products.
     *
     * @param products the products
     */
    public void setProducts(final List<Product> products) {
        this.products = products;
    }

    /**
     * Gets sub categories.
     *
     * @return the sub categories
     */
    public List<Category> getSubCategories() {
        return this.subCategories;
    }

    /**
     * Sets sub categories.
     *
     * @param subCategories the sub categories
     */
    public void setSubCategories(final List<Category> subCategories) {
        this.subCategories = subCategories;
    }

    /**
     * Gets parent.
     *
     * @return the parent
     */
    public Category getParent() {
        return this.parent;
    }

    /**
     * Sets parent.
     *
     * @param parent the parent
     */
    public void setParent(final Category parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return products + " " + subCategories + " " + parent;
    }
}
