package by.epam.training.initialize;

/**
 * The interface Initial starter.
 */
public interface InitialStarter {

    /**
     * Generate.
     */
    void generate();
}
