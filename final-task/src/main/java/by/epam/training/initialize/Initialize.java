package by.epam.training.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * The type Initialize.
 */
@Component
public class Initialize {

    private final InitialStarter starter;

    /**
     * Instantiates a new Initialize.
     *
     * @param starter the starter
     */
    @Autowired
    public Initialize(final InitialStarter starter) {
        this.starter = starter;
    }

    /**
     * Run.
     */
    @EventListener(ApplicationReadyEvent.class)
    public void run() {
        starter.generate();
    }

}
