package by.epam.training.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * The type User initial starter.
 */
@Component
@Profile("root")
public class UserInitialStarter implements InitialStarter {

    private final UserInitialize userInitialize;

    /**
     * Instantiates a new User initial starter.
     *
     * @param userInitialize the user initialize
     */
    @Autowired
    public UserInitialStarter(final UserInitialize userInitialize) {
        this.userInitialize = userInitialize;
    }

    @Override
    public void generate() {
        userInitialize.login();
    }

}
