package by.epam.training.initialize;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import by.epam.training.model.User;
import by.epam.training.repositories.UserRepository;

/**
 * The type User initialize.
 */
@Transactional
@Component
public class UserInitialize {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    /**
     * Instantiates a new User initialize.
     *
     * @param userRepository the user repository
     * @param encoder        the encoder
     */
    public UserInitialize(final UserRepository userRepository, final PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    /**
     * Login.
     */
    public void login() {
        userRepository.save(new User("root", encoder.encode("toor")));
    }
}
