package by.epam.training.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * The type Catalog and user initial starter.
 */
@Component
@Profile("all")
public class CatalogAndUserInitialStarter implements InitialStarter {

    private final UserInitialize userInitialize;
    private final CatalogInitialize catalogInitialize;

    /**
     * Instantiates a new Catalog and user initial starter.
     *
     * @param userInitialize    the user initialize
     * @param catalogInitialize the catalog initialize
     */
    @Autowired
    public CatalogAndUserInitialStarter(final UserInitialize userInitialize,
                                        final CatalogInitialize catalogInitialize) {
        this.userInitialize = userInitialize;
        this.catalogInitialize = catalogInitialize;
    }

    @Override
    public void generate() {
        userInitialize.login();
        catalogInitialize.catalog();
    }

}
