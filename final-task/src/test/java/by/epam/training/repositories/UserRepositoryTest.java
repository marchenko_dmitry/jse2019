package by.epam.training.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import by.epam.training.model.User;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * The type User repository test.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    private static final String username = "test";

    @Autowired
    private UserRepository repository;

    /**
     * Sets .
     */
    @Before
    public void setup() {
        repository.save(new User(username, ""));
    }

    /**
     * Test find by name.
     */
    @Test
    public void testFindByName() {
        final User user = repository.findByName(username).orElseThrow();
        then(user.getName()).isEqualTo(username);

        then(repository.findByName("toor").isPresent()).isFalse();
    }
}