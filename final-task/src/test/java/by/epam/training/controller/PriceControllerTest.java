package by.epam.training.controller;

import by.epam.training.dto.PriceDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.exception.BadRequestException;
import by.epam.training.exception.DeleteException;
import by.epam.training.facade.PriceFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.assertj.core.api.BDDAssertions.then;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * The type Price controller test.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WithMockUser(username = "root", password = "toor")
@SuppressWarnings("all")
public class PriceControllerTest {

    private static final String PRODUCT = "Product";
    private static final String BYN = "byn";
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    private final static String URL_API = "/price/";

    @MockBean
    private PriceFacade priceFacade;

    /**
     * Test validation price dto currency.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationPriceDtoCurrency() throws Exception {

        final String value = "b";

        final String message = mockMvc.perform(put(URL_API + "update")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(PriceDto.builder().currency(value).amount(BigDecimal.valueOf(100)).build())))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("rejected value [" + value + "]")).isTrue();
        then(message.contains("Currency must be 3 characters")).isTrue();
    }

    /**
     * Test validation price dto amount.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationPriceDtoAmount() throws Exception {

        int value = -100;

        final String message = mockMvc.perform(put(URL_API + "update")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(PriceDto.builder().currency(BYN).amount(BigDecimal.valueOf(value)).build())))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("rejected value [" + value + "]")).isTrue();
        then(message.contains("Amount must be positive")).isTrue();
    }

    /**
     * Test delete error.
     *
     * @throws Exception the exception
     */
    @Test
    public void testDeleteError() throws Exception {
        doThrow(DeleteException.createDeleteException("last price in product")).when(priceFacade).remove(Mockito.anyInt());

        final String message = mockMvc.perform(delete(URL_API + "delete/{id}", 2))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Can't delete last price in product")).isTrue();
    }

    /**
     * Test find by currency.
     *
     * @throws Exception the exception
     */
    @Test
    public void testFindByCurrency() throws Exception {

        Mockito.when(priceFacade.searchAll(Mockito.any(Pageable.class), Mockito.anyString()))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(get(URL_API + "currency/{currency}", BYN))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

    /**
     * Test find all by product.
     *
     * @throws Exception the exception
     */
    @Test
    public void testFindAllByProduct() throws Exception {

        Mockito.when(priceFacade.searchAll(Mockito.any(Pageable.class), Mockito.anyInt()))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(get(URL_API + "product/{id}", 0))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

    /**
     * Test find all by product error.
     *
     * @throws Exception the exception
     */
    @Test
    public void testFindAllByProductError() throws Exception {

        final int id = 111;

        Mockito.when(priceFacade.searchAll(Mockito.any(Pageable.class), Mockito.anyInt()))
                .thenThrow(BadRequestException.createBadRequestException(PRODUCT, id));

        final String message = mockMvc.perform(get(URL_API + "product/{id}", id))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Can't find " + PRODUCT + " with ID = " + id)).isTrue();
    }

    /**
     * Test find all by product json.
     *
     * @throws Exception the exception
     */
    @Test
    public void testFindAllByProductJson() throws Exception {

        Mockito.when(priceFacade.searchAll(Mockito.any(Pageable.class), Mockito.anyInt()))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(get(URL_API + "product")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(ProductDto.builder().id(2).name(PRODUCT).build())))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

    /**
     * Test find all by product json error.
     *
     * @throws Exception the exception
     */
    @Test
    public void testFindAllByProductJsonError() throws Exception {

        final int id = 111;

        Mockito.when(priceFacade.searchAll(Mockito.any(Pageable.class), Mockito.anyInt()))
                .thenThrow(BadRequestException.createBadRequestException(PRODUCT, id));

        final String message = mockMvc.perform(get(URL_API + "product")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(ProductDto.builder().id(id).name(PRODUCT).build())))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Can't find " + PRODUCT + " with ID = " + id)).isTrue();
    }

    /**
     * Test find between.
     *
     * @throws Exception the exception
     */
    @Test
    public void testFindBetween() throws Exception {

        Mockito.when(priceFacade.searchAll(Mockito.any(Pageable.class), Mockito.anyString(),
                Mockito.any(BigDecimal.class), Mockito.any(BigDecimal.class)))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(get(URL_API + "between/{currency}/{from}/{to}", BYN, 0, 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

}
