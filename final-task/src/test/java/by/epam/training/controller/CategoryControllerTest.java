package by.epam.training.controller;

import by.epam.training.dto.CategoryDto;
import by.epam.training.facade.CategoryFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.BDDAssertions.then;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * The type Category controller test.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WithMockUser(username = "root", password = "toor")
@SuppressWarnings("all")
public class CategoryControllerTest {

    private static final String CATEGORY = "category";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    private final static String URL_API = "/category/";

    @MockBean
    private CategoryFacade categoryFacade;

    /**
     * Test create.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCreate() throws Exception {

        final CategoryDto category = CategoryDto.builder().id(2).name(CATEGORY).parentId(1)
                .subCategories(new ArrayList<>(Arrays.asList(3, 4, 5))).build();

        Mockito.when(categoryFacade.create(Mockito.any(CategoryDto.class)))
                .thenReturn(category);

        mockMvc.perform(post(URL_API + "create")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CategoryDto.builder().name(CATEGORY).parentId(1)
                        .subCategories(new ArrayList<>(Arrays.asList(3, 4, 5))).build())))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(category.getId())))
                .andExpect(jsonPath("$.name", is(category.getName())))
                .andExpect(jsonPath("$.parentId", is(category.getParentId())))
                .andExpect(jsonPath("$.subCategories", hasSize(category.getSubCategories().size())));
    }

    /**
     * Test validation product dto name.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationProductDtoName() throws Exception {

        final String message = mockMvc.perform(put(URL_API + "update")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CategoryDto.builder().name("").build())))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Category's name must be not empty")).isTrue();
    }
}
