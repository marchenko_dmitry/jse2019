package by.epam.training.controller;

import by.epam.training.dto.CategoryDto;
import by.epam.training.dto.PriceDto;
import by.epam.training.dto.ProductCreationDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.exception.DeleteException;
import by.epam.training.facade.ProductFacade;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.BDDAssertions.then;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * The type Product controller test.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WithMockUser(username = "root", password = "toor")
@SuppressWarnings("all")
public class ProductControllerTest {

    private static final String BYN = "byn";
    private static final String PRODUCT = "product";
    private static final String CATEGORY = "category";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    private final static String URL_API = "/product/";

    @MockBean
    private ProductFacade productFacade;

    /**
     * Test create.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCreate() throws Exception {

        final int id = 1;

        final ProductDto productDto = ProductDto.builder()
                .id(id)
                .name(PRODUCT)
                .prices(new ArrayList<>(Collections.singletonList(id)))
                .categories(new ArrayList<>(Collections.singletonList(id)))
                .build();

        Mockito.when(productFacade.create(Mockito.any(ProductCreationDto.class)))
                .thenReturn(productDto);

        mockMvc.perform(post(URL_API + "/create")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(ProductCreationDto.builder()
                        .name(PRODUCT)
                        .amount(BigDecimal.valueOf(1))
                        .categoryId(1)
                        .currency(BYN)
                        .build()
                )))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(productDto.getId())))
                .andExpect(jsonPath("$.name", is(productDto.getName())))
                .andExpect(jsonPath("$.prices", hasSize(productDto.getPrices().size())))
                .andExpect(jsonPath("$.categories", hasSize(productDto.getCategories().size())));
    }

    /**
     * Test validation product dto name.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationProductDtoName() throws Exception {

        final String message = mockMvc.perform(put(URL_API + "update")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CategoryDto.builder().name("").build())))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Product's name must be not empty")).isTrue();
    }

    /**
     * Test validation product creation dto name.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationProductCreationDtoName() throws Exception {

        final ProductCreationDto dto = ProductCreationDto.builder()
                .name("")
                .amount(BigDecimal.valueOf(1))
                .categoryId(1)
                .currency(BYN)
                .build();

        final String message = mockMvc.perform(post(URL_API + "create")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Product's name must be not empty")).isTrue();
    }

    /**
     * Test validation product creation dto amount.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationProductCreationDtoAmount() throws Exception {

        final ProductCreationDto dto = ProductCreationDto.builder()
                .name(PRODUCT)
                .amount(BigDecimal.valueOf(-1))
                .categoryId(1)
                .currency(BYN)
                .build();

        final String message = mockMvc.perform(post(URL_API + "create")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Amount must be positive")).isTrue();
    }

    /**
     * Test validation product creation dto currency.
     *
     * @throws Exception the exception
     */
    @Test
    public void testValidationProductCreationDtoCurrency() throws Exception {

        final ProductCreationDto dto = ProductCreationDto.builder()
                .name(PRODUCT)
                .amount(BigDecimal.valueOf(1))
                .categoryId(1)
                .currency("dollars")
                .build();

        final String message = mockMvc.perform(post(URL_API + "create")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Currency must be 3 characters")).isTrue();
    }

    /**
     * Test validation product creation dto category id.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_validation_ProductCreationDto_categoryId() throws Exception {

        final ProductCreationDto dto = ProductCreationDto.builder()
                .name(PRODUCT)
                .amount(BigDecimal.valueOf(1))
                .categoryId(-1)
                .currency(BYN)
                .build();

        final String message = mockMvc.perform(post(URL_API + "create")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Category's ID must be positive")).isTrue();
    }

    /**
     * Test find all by category.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_findAllByCategory() throws Exception {

        Mockito.when(productFacade.findAll(Mockito.any(Pageable.class), Mockito.anyInt()))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(get(URL_API + "category/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

    /**
     * Test find all by price json.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_findAllByPriceJson() throws Exception {

        Mockito.when(productFacade.findAll(Mockito.any(Pageable.class), Mockito.any(PriceDto.class)))
                .thenReturn(new ArrayList<>());

        mockMvc.perform(get(URL_API + "price")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(PriceDto.builder().currency(BYN).amount(BigDecimal.valueOf(100)).build())))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

    /**
     * Test add category.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_add_category() throws Exception {

        final ProductDto productDto = ProductDto.builder()
                .id(1)
                .name(PRODUCT)
                .prices(new ArrayList<>(Arrays.asList(1, 2)))
                .categories(new ArrayList<>(Arrays.asList(1, 2)))
                .build();

        Mockito.when(productFacade.add(Mockito.anyInt(), Mockito.anyInt())).thenReturn(productDto);

        mockMvc.perform(put(URL_API + "{id}/add/category/2", productDto.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CategoryDto.builder().name(CATEGORY).build())))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(productDto.getId())))
                .andExpect(jsonPath("$.name", is(productDto.getName())))
                .andExpect(jsonPath("$.prices", hasSize(productDto.getPrices().size())))
                .andExpect(jsonPath("$.categories", hasSize(productDto.getCategories().size())));
    }

    /**
     * Test remove category.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_remove_category() throws Exception {

        final ProductDto productDto = ProductDto.builder()
                .id(1)
                .name(PRODUCT)
                .prices(new ArrayList<>(Arrays.asList(1, 2)))
                .categories(new ArrayList<>(Arrays.asList(1, 2)))
                .build();

        Mockito.when(productFacade.removeCategory(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(productDto);

        mockMvc.perform(put(URL_API + "{id}/remove/category/3", productDto.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CategoryDto.builder().name(CATEGORY).build())))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(productDto.getId())))
                .andExpect(jsonPath("$.name", is(productDto.getName())))
                .andExpect(jsonPath("$.prices", hasSize(productDto.getPrices().size())))
                .andExpect(jsonPath("$.categories", hasSize(productDto.getCategories().size())));
    }

    /**
     * Test remove category 400.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_remove_category_400() throws Exception {

        Mockito.when(productFacade.removeCategory(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(DeleteException.createDeleteException("last Category in Product"));

        final String message = mockMvc.perform(put(URL_API + "{id}/remove/category/999", 1)
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(CategoryDto.builder().name(CATEGORY).build())))
                .andExpect(status().isBadRequest())
                .andReturn().getResolvedException().getMessage();

        then(message.contains("Can't delete last Category in Product")).isTrue();
    }

    /**
     * Test add price.
     *
     * @throws Exception the exception
     */
    @Test
    public void test_add_price() throws Exception {

        final ProductDto productDto = ProductDto.builder()
                .id(1)
                .name(PRODUCT)
                .prices(new ArrayList<>(Arrays.asList(1, 2)))
                .categories(new ArrayList<>(Arrays.asList(1, 2)))
                .build();

        Mockito.when(productFacade.add(Mockito.any(PriceDto.class), Mockito.anyInt()))
                .thenReturn(productDto);

        mockMvc.perform(put(URL_API + "{id}/add/price", productDto.getId())
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(PriceDto.builder()
                        .currency(BYN)
                        .amount(BigDecimal.valueOf(1))
                        .build())))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(productDto.getId())))
                .andExpect(jsonPath("$.name", is(productDto.getName())))
                .andExpect(jsonPath("$.prices", hasSize(productDto.getPrices().size())))
                .andExpect(jsonPath("$.categories", hasSize(productDto.getCategories().size())));
    }
}
