package by.epam.training.facade;

import org.junit.Test;
import org.mockito.Mockito;
import by.epam.training.dto.PriceDto;
import by.epam.training.dto.ProductDto;
import by.epam.training.exception.DeleteException;
import by.epam.training.converter.PriceConverter;
import by.epam.training.converter.ProductConverter;
import by.epam.training.model.Category;
import by.epam.training.model.Price;
import by.epam.training.model.Product;
import by.epam.training.repositories.PriceRepository;
import by.epam.training.repositories.ProductRepository;
import by.epam.training.services.PriceService;
import by.epam.training.services.ProductService;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.Mockito.mock;

/**
 * The type Product facade test.
 */
public class ProductFacadeTest {

    private static final String FOUND = "found";
    private static final String NAME = "name";
    private final static String BYN = "BYN";

    private ProductRepository productRepository = mock(ProductRepository.class);
    private CategoryFacade categoryFacade = mock(CategoryFacade.class);

    private ProductFacade testSubject = new ProductFacade(
            new ProductService(productRepository),
            categoryFacade,
            new ProductConverter(),
            new PriceConverter(),
            new PriceService(mock(PriceRepository.class))
    );


    /**
     * Test add price.
     */
    @Test
    public void testAddPrice() {

        final Price price = new Price(BigDecimal.valueOf(100), Currency.getInstance(BYN));
        price.setId(2);

        final Product found = new Product(FOUND);
        found.setId(1);
        found.getPrices().add(new Price());

        final Product saved = new Product();
        saved.setId(1);
        saved.getPrices().add(new Price());
        saved.getPrices().add(price);
        price.setProduct(saved);

        Mockito.when(productRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(found));
        Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(saved);

        final ProductDto dto = testSubject.add(PriceDto.builder().currency(BYN)
                .amount(BigDecimal.valueOf(100)).build(), 1);

        then(dto.getPrices().size()).isEqualTo(2);
    }

    /**
     * Test add category.
     */
    @Test
    public void testAddCategory() {

        final Category category = new Category(NAME);
        category.setId(2);

        final Product found = new Product(FOUND);
        found.setId(1);
        found.getCategories().add(new Category());

        final Product saved = new Product();
        saved.setId(1);
        saved.getCategories().add(new Category());
        saved.getCategories().add(category);

        Mockito.when(productRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(found));
        Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(saved);
        Mockito.when(categoryFacade.get(Mockito.anyInt())).thenReturn(category);

        final ProductDto dto = testSubject.add(2, 1);

        then(dto.getCategories().size()).isEqualTo(2);

    }

    /**
     * Test remove.
     */
    @Test(expected = DeleteException.class)
    public void testRemove() {
        final Product found = new Product(FOUND);
        found.setId(1);
        found.getCategories().add(new Category());

        Mockito.when(productRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(found));

        testSubject.removeCategory(2, 1);
    }

}