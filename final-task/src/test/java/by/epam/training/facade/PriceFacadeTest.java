package by.epam.training.facade;

import org.junit.Test;
import org.mockito.Mockito;
import by.epam.training.exception.DeleteException;
import by.epam.training.converter.PriceConverter;
import by.epam.training.model.Price;
import by.epam.training.model.Product;
import by.epam.training.repositories.PriceRepository;
import by.epam.training.services.PriceService;
import by.epam.training.services.ProductService;

import java.util.Optional;

import static org.mockito.Mockito.mock;

/**
 * The type Price facade test.
 */
public class PriceFacadeTest {

    private PriceRepository priceRepository = mock(PriceRepository.class);

    private PriceFacade testSubject = new PriceFacade(
            new PriceService(priceRepository),
            new PriceConverter(),
            mock(ProductService.class)
    );

    /**
     * Test delete.
     */
    @Test(expected = DeleteException.class)
    public void testDelete() {
        Mockito.when(priceRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(new Price()));
        Mockito.when(priceRepository.countAllByProduct(Mockito.any(Product.class))).thenReturn(1);
        testSubject.remove(0);
    }
}