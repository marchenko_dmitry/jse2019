package by.epam.training.facade;

import by.epam.training.dto.CategoryDto;
import by.epam.training.converter.CategoryConverter;
import by.epam.training.model.Category;
import by.epam.training.repositories.CategoryRepository;
import by.epam.training.services.CategoryService;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.Mockito.mock;

/**
 * The type Abstract facade test.
 */
public class AbstractFacadeTest {

    private CategoryRepository categoryRepository = mock(CategoryRepository.class);
    private CategoryFacade testSubject = new CategoryFacade(
            new CategoryConverter(new CategoryService(categoryRepository)),
            new CategoryService(categoryRepository)
    );


    /**
     * Test find all.
     */
    @Test
    public void testFindAll() {

        final Category category1 = new Category("1");
        final Category category2 = new Category("2");

        final Page page = new PageImpl<>(Arrays.asList(category1, category2));

        Mockito.when(categoryRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);

        final List<CategoryDto> dto = testSubject.searchAll(PageRequest.of(1, 1));

        then(dto.get(0).getName()).isEqualTo(category1.getName());
        then(dto.get(1).getName()).isEqualTo(category2.getName());
    }

}