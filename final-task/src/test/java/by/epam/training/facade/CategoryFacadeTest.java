package by.epam.training.facade;

import org.junit.Test;
import org.mockito.Mockito;
import by.epam.training.dto.CategoryDto;
import by.epam.training.exception.DeleteException;
import by.epam.training.exception.NotFoundException;
import by.epam.training.converter.CategoryConverter;
import by.epam.training.model.Category;
import by.epam.training.model.Product;
import by.epam.training.repositories.CategoryRepository;
import by.epam.training.services.CategoryService;

import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.Mockito.mock;

/**
 * The type Category facade test.
 */
public class CategoryFacadeTest {

    private static final String NAME = "name";

    private CategoryRepository categoryRepository = mock(CategoryRepository.class);
    private CategoryFacade testSubject = new CategoryFacade(
            new CategoryConverter(new CategoryService(categoryRepository)),
            new CategoryService(categoryRepository)
    );

    /**
     * Test create.
     */
    @Test
    public void testCreate() {

        final Category category = new Category(NAME);
        category.setId(1);

        final CategoryDto sourceDto = CategoryDto.builder().name(NAME).build();
        then(sourceDto.getId()).isNull();

        Mockito.when(categoryRepository.save(Mockito.any(Category.class))).thenReturn(category);

        final CategoryDto dto = testSubject.create(sourceDto);
        then(dto.getName()).isEqualTo(sourceDto.getName());
        then(dto.getId()).isEqualTo(category.getId());
    }

    /**
     * Test delete has sub category.
     */
    @Test(expected = DeleteException.class)
    public void testDeleteHasSubCategory() {
        final Category category = new Category();
        category.getSubCategories().add(new Category());

        Mockito.when(categoryRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(category));
        testSubject.remove(0);
    }

    /**
     * Test delete has product.
     */
    @Test(expected = DeleteException.class)
    public void testDeleteHasProduct() {
        final Category category = new Category();
        category.getProducts().add(new Product());

        Mockito.when(categoryRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(category));
        testSubject.remove(0);
    }

    /**
     * Test delete has not found exception.
     */
    @Test(expected = NotFoundException.class)
    public void testDeleteHasNotFoundException() {
        final Category category = new Category();
        category.getProducts().add(new Product());

        Mockito.when(categoryRepository.findById(Mockito.anyInt())).thenReturn(Optional.empty());
        testSubject.remove(0);
    }


}